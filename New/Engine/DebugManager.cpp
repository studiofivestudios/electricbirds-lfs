/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Debug Manager (Code File)
*/

#include "DebugManager.h"

#include <random>

void Charon::DebugManager::Initialise() {
}

void Charon::DebugManager::OnQuit() {
}

void Charon::DebugManager::OnUpdate(const long long delta_t) {
	devTitleUpdateTimer += (GameEngine::GetInstance()->GetCurrentTime() - lastTime);

	if (devTitleUpdateTimer >= devTitleUpdateInterval)
	{
		devTitleUpdateTimer = 0;

		SetDevelopmentConsoleTitle();
	}

	lastTime = GameEngine::GetInstance()->GetCurrentTime();
}

void Charon::DebugManager::SetDevelopmentConsoleTitle()
{
	std::stringstream ss;
	std::string title;
	ss << GameEngine::GetInstance()->GetApplicationName();
	ss << "- Engine Built on " << GameEngine::GetInstance()->GetEngineBuildDate() << " at " << GameEngine::GetInstance()->GetEngineBuildTime();
	ss << " | Delta Time: " << GameEngine::GetInstance()->GetDeltaTime() << "ms";
	ss << " | FPS: " << GameEngine::GetInstance()->GetSystem<RenderManager>()->GetFramesPerSecond();
	ss << " | Running For: " << GameEngine::GetInstance()->GetAppRunTime() / 1000 << "s";
	title = ss.str();

	SetWindowTextW(GameEngine::GetInstance()->GetDebugWindowHandle(), std::wstring(title.begin(), title.end()).c_str());
}