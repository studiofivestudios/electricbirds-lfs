#include "GameManager.h"

namespace GameInterface {
	class Cube;
}

void GameManager::OnEnabled()
{
	bootLogo = GameEngine::GetInstance()->GetSystem<SceneManager>()->GetActiveScene()->AddEntity<Cube>("Boot Logo");
	anotherLogo = GameEngine::GetInstance()->GetSystem<SceneManager>()->GetActiveScene()->AddEntity<Cube>("Another Logo");
	
	bootLogo->SetActive(true);
	anotherLogo->SetActive(true);

	GameEngine::GetInstance()->GetSystem<SceneManager>()->GetActiveScene()->GetCamera()->GetViewMatrix()->SetPosition(0,0,0);
	
	bootLogo->SetPosition(Vector3{ -1,1, 4 });
	anotherLogo->SetPosition(Vector3{ 1,0, 4 });

	/*
	 * Known bug: must set the position above before calling this otherwise XMMatrixLookAtLH will
	 *	cause a crash (no idea why, since the position is default-set to Vector3Zero...).
	 */
	GameEngine::GetInstance()->GetSystem<SceneManager>()->GetActiveScene()->GetCamera()->GetViewMatrix()->SetLookAtPosition(bootLogo->GetPosition());
}

void GameManager::OnUpdate()
{
	if (bootLogo && anotherLogo)
	{
		float deltaTime = GameEngine::GetInstance()->GetDeltaTimef() / 1000.0f;
		float modVal = 1.1f * deltaTime;
		
		anotherLogo->SetRotation(Vector3{ modVal, modVal, 0}, ThreeDModel::Operation::Add);
		bootLogo->SetRotation(Vector3{ 0, modVal, 0 }, ThreeDModel::Operation::Add);

		/*anotherLogo->SetRotation(Vector3{ 0, 0.085f, 0 }, ThreeDModel::Operation::Add);
		bootLogo->SetRotation(Vector3{ 0.085f, 0, 0 }, ThreeDModel::Operation::Add);*/
	}
}