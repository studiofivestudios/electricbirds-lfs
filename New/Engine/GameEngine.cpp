/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Engine core (Code File).
*/

#include "GameEngine.h"

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

void Charon::GameEngine::Initialise(const HWND &consoleHwnd, const char* _appName, const char* _gameBuildDate, const char* _gameBuildTime,
	Delegate* onEngineIntermediateLoadCallback, Delegate* onEngineInitialisedCallback) {
	debugConsoleWindowHwnd = consoleHwnd;

	engineBuildDate = __DATE__;
	engineBuildTime = __TIME__;
	gameBuildDate = _gameBuildDate;
	gameBuildTime = _gameBuildTime;
	appName = _appName;

/*
*	If this isn't a development build, hide the "external" debug console, else show it.
*	The external console is different to the internal one because it's independent from the engine systems.
*/
#ifdef _DEBUG
	debugBuild = true;
	std::cout << "Charon : Debug build detected, the debug console will not be hidden." << std::endl;
	ShowWindow(debugConsoleWindowHwnd, SW_RESTORE);
#else
	ShowWindow(debugConsoleWindowHwnd, SW_HIDE);
#endif
	
	appStartTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

	InitWindow();
	if (InitDirectX()) {
		std::cout << "Charon : Initialised DirectX." << std::endl;

		// All the engine systems are initialised here.
		systemPointers.push_back(std::make_unique<XMLParser>());
		systemPointers.push_back(std::make_unique<DebugManager>());
		systemPointers.push_back(std::make_unique<FileSystemManager>());
		systemPointers.push_back(std::make_unique<SceneManager>());
		systemPointers.push_back(std::make_unique<ShaderManager>());
		systemPointers.push_back(std::make_unique<RenderManager>(swapchainInterface.GetAddressOf(), deviceInterface.GetAddressOf(), deviceContext.GetAddressOf()));

		threadMainUpdateLoop = std::thread(&Charon::GameEngine::CentralUpdateLoop, this);
		threadPerformRender = std::thread(&Charon::RenderManager::RequestRender, GetSystem<Charon::RenderManager>());

		// Doing this is OK, the Windows NT kernel automatically destroys these threads when the main application (the game) exits.
		// We trigger these threads to terminate once we've started shutting down the engine anyway.
		threadMainUpdateLoop.detach();
		threadPerformRender.detach();

		// For the below we just use a separate thread for initialisation, but they need to be blocking!

		std::thread(&Charon::ShaderManager::Initialise, GetSystem<Charon::ShaderManager>()).join();
		// Initialise the scene manager system.

		if (onEngineIntermediateLoadCallback != nullptr)
			onEngineIntermediateLoadCallback->Callback();
		/*
		* Now show the loading scene and then initialise the rest of the systems.
		* - Initialise:
		*			- Debug Console
		*			- Input
		*			- Sound
		*			- Networking
		*			- Player Preferences
		*/

		if (onEngineInitialisedCallback != nullptr)
			onEngineInitialisedCallback->Callback();

		// Main window "keep alive" loop: http://www.directxtutorial.com/Lesson.aspx?lessonid=9-1-3

		MSG msg;

		engineReady = true;

		// wait for the next message in the queue, store the result in 'msg'
		while (GetMessage(&msg, NULL, 0, 0))
		{
			// translate keystroke messages into the right format
			TranslateMessage(&msg);

			// send the message to the WindowProc function
			DispatchMessage(&msg);
		}
	}
}

Charon::GameEngine* Charon::GameEngine::GetInstance()
{
	//std::lock_guard<std::mutex> lock(padlock);

	if (!instance)
		instance = std::make_unique<GameEngine>();

	return instance.get();
}

void Charon::GameEngine::CentralUpdateLoop() {
	while (!exiting)
	{
		//std::cout << "Charon : Delta time is: " << lastDeltaTime << "\n";

		/*
		* Some systems are on the main thread, other systems are grouped together via a parent system (such as the scene system), and the renderer is on
		*	its own thread.
		* 
		* When we've told the renderer it can update, an update isn't guaranteed straight away- we try to Present, and if it fails we know DirectX doesn't want the frame.
		*	This is done to prevent situations where only part of a scene is rendered because it's Present'ing before the engine is ready (hence TellCanUpdate()).
		*	
		*/

		if (!exiting && IsEngineReady()) {
			currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

			GetSystem<Charon::DebugManager>()->OnUpdate(currentDeltaTime);
			GetSystem<Charon::RenderManager>()->OnUpdate(currentDeltaTime);

			if (GetSystem<SceneManager>()->GetActiveScene() != nullptr)
			{
				if (currentDeltaTime <= 1000)
				{
					for (std::shared_ptr<EntityBase> entity : GetSystem<SceneManager>()->GetActiveScene()->GetAllEntities())
					{
						if (entity.get()->GetState())
							entity.get()->OnUpdate();
					}

					// FixedUpdate is part of the main render loop in the RenderManager.

					for (std::shared_ptr<EntityBase> entity : GetSystem<SceneManager>()->GetActiveScene()->GetAllEntities())
					{
						if (entity.get()->GetState())
							entity.get()->OnLateUpdate();
					}
				}

				// Rendering needs to be last!
				GetSystem<Charon::RenderManager>()->TellCanUpdate();
			}
		}
	}

	// This can only be reached when the engine is shutting down, so terminate the thread.
	std::exit(0);
}

/*
*	Shows a message box displaying a message, and then quits when the user presses "OK" with the given exit code.
*/
void Charon::GameEngine::Quit(const int& exitCode, const std::string& msgBoxTitle, const std::string& msgBoxBody) {
	std::cout << "Charon : Exiting." << std::endl;
	int i = 0;
	
	if (MessageBox(NULL, msgBoxBody.c_str(), msgBoxTitle.c_str(), MB_OK)) {
		Quit(exitCode);
	}
}

/*
* Destroys all systems and then exits, using the given exit code.
*/
void Charon::GameEngine::Quit(const int& exitCode) {
	exiting = true;
	if (GetSystem<Charon::DebugManager>())
		GetSystem<Charon::DebugManager>()->OnQuit();

	if (GetSystem<RenderManager>())
		GetSystem<RenderManager>()->Release();

	exit(exitCode);
}
bool Charon::GameEngine::IsExiting()
{
	/*std::lock_guard<std::mutex> lock(padlock);*/ 
	return exiting;
}
/*
*	Is called by the RenderManager when a frame has been Present()'ed to the screen, thus this sets the engine's delta time value.
*/
void Charon::GameEngine::OnRendererPresentComplete() {
	std::lock_guard<std::mutex> lock(padlock);

	currentDeltaTime = currentTime - lastUpdatedDeltaTime;

	if (currentDeltaTime > GetSystem<DebugManager>()->GetSystemSlowdownThreshold())
	{
		std::cout << "Charon : Warning, a possible engine slow-down has been detected (dt of " << currentDeltaTime << " exceeds threshold of " << GetSystem<DebugManager>()->GetSystemSlowdownThreshold() << "ms)!\n";
	}

	lastDeltaTime.store(currentDeltaTime);
	lastUpdatedDeltaTime.store(currentTime);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM windowParams, LPARAM lParams) {
	switch (msg)
	{
	case WM_CLOSE:
		GameEngine::GetInstance()->Quit(0);
		return 0;

	case WM_KEYDOWN:
	{
		switch (windowParams)
		{
		case VK_ESCAPE:
			GameEngine::GetInstance()->Quit(0);
			return 0;
		}
	}

	default:
		return DefWindowProc(hwnd, msg, windowParams, lParams);
	};

	return 0;
}


// With help fromm http://www.directxtutorial.com/Lesson.aspx?lessonid=9-1-3
void Charon::GameEngine::InitWindow() {
	WNDCLASS windowClass = { 0 };
	std::ostringstream _windowTitle;
	std::string windowTitle;

	_windowTitle << GetApplicationName() << " - Built " << GetGameBuildDate() << " at " << GetGameBuildTime();
	windowTitle = _windowTitle.str();

	windowClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hInstance = NULL;
	windowClass.lpfnWndProc = WndProc;
	windowClass.lpszClassName = windowTitle.c_str();
	windowClass.style = CS_HREDRAW | CS_VREDRAW;

	if (RegisterClass(&windowClass)) {
		gameWindowHwnd = CreateWindow(windowTitle.c_str(),
			windowTitle.c_str(),
			WS_OVERLAPPEDWINDOW,
			NULL,
			NULL,
			1024,
			768,
			NULL,
			NULL,
			NULL,
			NULL);

		std::cout << "Charon : Created game window." << std::endl;

		ShowWindow(gameWindowHwnd, SW_RESTORE);
	}
	else {
		Quit(-1, "Fatal Error", "Unable to create window!");
	}
}

bool Charon::GameEngine::InitDirectX(const bool &enableDeviceDebug) {
	DXGI_SWAP_CHAIN_DESC scd;
	D3D11_VIEWPORT viewport;

	std::cout << "Charon : Creating DirectX device." << std::endl;

	GetWindowRect(gameWindowHwnd, &gameWindowRect);

	ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));

	// Only need to use a single buffer for now.

	scd.BufferCount = 1;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; 
	scd.OutputWindow = gameWindowHwnd;
	scd.SampleDesc.Count = 4; // MSAA
	scd.Windowed = TRUE;
	scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH | DXGI_SWAP_EFFECT_DISCARD;

	HRESULT dxResult = D3D11CreateDeviceAndSwapChain(
		NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		debugBuild && enableDeviceDebug ? D3D11_CREATE_DEVICE_DEBUG : NULL, // TODO: fix D3D11_CREATE_DEVICE_DEBUG causing this to fail on Windows 10.
		NULL,
		NULL,
		D3D11_SDK_VERSION,
		&scd,
		swapchainInterface.GetAddressOf(),
		deviceInterface.GetAddressOf(),
		NULL,
		deviceContext.GetAddressOf()
	);

	if (dxResult != S_OK) {
		if (debugBuild && enableDeviceDebug) {
			std::cout << "Charon : DirectX device and swapchain creation failed! Is the \"Graphics Tools\" Windows optional feature enabled?" << std::endl
				<< "Charon : DirectX is being reinitialised with device debug mode turned off!" << std::endl;
			return InitDirectX(false);
		}
		else // Exit if not a debug build, or if we have tried to reinitialise but the device creation still fails.
		{
			std::cout << "Charon : DirectX device and swapchain creation failed!" << std::endl;
			Quit(-1, "Fatal Error", "Cannot find DirectX device!");
		}
	}
	else {
		std::cout << "Charon : Created and hooked onto DirectX device." << std::endl;

		ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;
		// per https://stackoverflow.com/a/20006146
		viewport.Width = static_cast<float>(gameWindowRect.right - gameWindowRect.left);
		viewport.Height = static_cast<float>(gameWindowRect.bottom - gameWindowRect.top);

		gameWindowDimensions.x = viewport.Width;
		gameWindowDimensions.y = viewport.Height;

		deviceContext.Get()->RSSetViewports(1, &viewport);

		std::cout << "Charon : Set viewport." << std::endl;

		return true;
	}

	return false;
}

