/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Scene Manager (Header).
*/

#ifndef SCENEMANAGERDEF_H
#define SCENEMANAGERDEF_H

#include <vector>
#include <unordered_map>
#include <iostream>
#include <string>

#include "EngineSystem.h"
#include "Scene.h"

namespace Charon {
	class SceneManager : public EngineSystem
	{
	public:
		SceneManager() = default;
		~SceneManager() = default;

		Charon::Scene* CreateNewScene(const std::string& name, const bool &setActive = false);

		void SetActiveScene(const std::string& name);
		void SetActiveScene(Scene* scene);

		Charon::Scene* GetActiveScene();
		Charon::Scene* GetScene(const std::string& name);

		std::vector<Scene*> GetAllScenes();


	private:
		std::unordered_map<std::string, std::shared_ptr<Scene>> scenes;
	};
}
#endif