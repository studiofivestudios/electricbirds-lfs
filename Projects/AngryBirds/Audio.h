/*
	This class is responsible for all game audio and queuing.

	Created : 23rd March 2018 @ 23:05
	Author : Alexander Stopher

	Notes:
		Thread Safe:
			All audio is played on a separate thread to the main game otherwise it will block other game functions- SFML does this automatically.
			No chance of stack/heap corruption as we do not access anything outside of this class (apart from the sound library).
		Changed from irrKlang to SFML Audio due to accessibility issues (couldn't manage to access the irrKlang instance globally within the class, can do so within SFML).
*/

#ifndef AUDIO_H
#define AUDIO_H

#include <SFML/Audio.hpp>

#include "Scene.h"

class Audio
{
public:
	Audio();
	~Audio();

	bool initialize(Scene* scene);														// Initialize the audio system.
	void onUpdate();																	// Updates the audio system.
	bool hasLoaded();																	// Has the audio system been loaded yet?

	int queueAudio(const char* file_path, bool loop, bool overlap, bool music);			// Queues an audio file and returns its position in the queue. Position is used to stop the audio.
	void pauseAudio(int num_in_queue);													// Pauses the audio. Does not destroy the object;
	void changeVolume(int queue_pos, float desired_vol);								// Change the volume of the audio.
	void changeVolumeOfAllPriority(float desired_vol);									// Change the volume of all priority audio. Useful for lowering the game volume when we're paused, etc.
	void stopAudio(int num_in_queue);													// Stop the audio object at the specified queue position.
	int isFileInQueue(const char* file_name);											// Check whether the specified file is within the queue. Returns queue position.
	void stopAllNonPriority();															// Stop all audio that is not flagged as a priority.
	void stopAll();																		// Stop all sounds, including priority.
	void toggleMute(bool mute);																	// Toggle audio mute (does not stop the audio from playing).

private:
	static const int NUM_QUEUE_SIZE = 128;												// The size of the queue.

	Scene* scene = nullptr;																// A pointer to the Scene class.

	bool loaded = false;																// Is the audio system loaded?

	struct audio_object
	{
		const char* file_path;															// The file location of the audio file, relative to the game directory.
		bool loop;																		// Is the file allowed to loop, or should we stop playing when it has finished playing?
		bool overlap;																	// Allow the file to be played over other audio.
		bool priority;																	// Is this audio game music?
		sf::Music* instance;															// The instance of sfml::Music that relates to this audio.
		bool has_played = false;														// Has the audio file been executed (or is currently being executed)?
		float pre_muted_level;															// The audio level before audio was muted.
	};

	sf::SoundBuffer* engine = nullptr;													// Pointer to the SFML sound engine.
	void playAudio(int queue_pos);														// Play the specified audio file.

	audio_object queue[NUM_QUEUE_SIZE];													// The queue of audio that is to be played.
};

#endif