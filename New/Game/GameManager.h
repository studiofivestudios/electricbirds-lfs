#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include "Cube.h"
#include "EntityBase.h"
#include "GameEngine.h"

using namespace GameInterface;

class GameManager : public EntityBase
{
public:
	enum class GameStates
	{
		Playing = 0,
		MainMenu = 1,
		LevelSelection = 2,
		Paused = 3,
		GameOver = 4,
	};

	GameManager(const unsigned int& _entityId, const std::string& _name, EntityBase* parent = nullptr) : EntityBase(_entityId, _name, parent) { };
	~GameManager() { OnDestroy(); };

	void OnEnabled();
	void OnUpdate();

	void SetGameState(const GameManager::GameStates& state) { currentState = state; };
private:
	GameStates currentState = GameStates::MainMenu;

	GameInterface::Cube* bootLogo = nullptr;
	GameInterface::Cube* anotherLogo = nullptr;
};
#endif