/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Shader Manager (Code File).
*/

#include "ShaderManager.h"

void Charon::ShaderManager::Initialise()
{
    std::shared_ptr<rapidxml::xml_document<>> shaderList = GameEngine::GetInstance()->GetSystem<XMLParser>()->ParseXML("..\\Game\\Shaders\\shaderlist.xml");

    if (shaderList != nullptr)
    {
        std::cout << "Charon : Loaded shader list!\n";

        for (rapidxml::xml_node<>* node = shaderList->first_node(); node; node = node->next_sibling())
        {
            if (std::strcmp(node->name(), "file") == 0)
            {
                if (node->first_attribute("name"))
                {
                    if (std::strcmp(node->first_attribute("name")->value(), "Shader List") == 0)
                    {
                        for (rapidxml::xml_node<>* list = node->first_node(); list; list = list->next_sibling())
                        {
                            if (std::strcmp(list->name(), "shader") == 0)
                            {
                                if (list->first_attribute("filename") && list->first_attribute("name") && list->first_attribute("type") &&
                                    list->first_attribute("hlslversion") && list->first_attribute("entrypoint"))
                                {
                                    shaders.push_back(std::make_shared<ShaderDef>(
                                        list->first_attribute("name")->value(), list->first_attribute("entrypoint")->value(), list->first_attribute("hlslversion")->value()));

                                    GetShader(list->first_attribute("name")->value())->Load(GetShaderCompletePath(list->first_attribute("filename")->value()),
                                        list->first_attribute("name")->value(), GetShaderType(list->first_attribute("type")->value()));
                                }
                                else
                                    std::cout << "Charon : Error, tried to load shader but the node isn't valid (one or more required attributes are missing)!\n";
                            }
                            else
                                std::cout << "Charon : Error, tried to load shader but the node isn't valid (\"shader\" node is missing)!\n";
                        }
                    }
                    else
                        std::cout << "Charon : Error, loaded shader list but file isn't valid (file \"name\" attribute has an unexpected value)!\n";
                }
                else
                    std::cout << "Charon : Error, loaded shader list but file isn't valid (\"file\" node is missing the \"name\" attribute)!\n";
            }
            else
                std::cout << "Charon : Error, loaded shader list but file isn't valid (\"file\" node is missing)!\n";
        }
    }

    for (std::shared_ptr<ShaderDef> shader : shaders)
        GameEngine::GetInstance()->GetSystem<RenderManager>()->CreateShader(shader.get());

    GameEngine::GetInstance()->GetSystem<RenderManager>()->InitGraphics();
}

SHADER_TYPE Charon::ShaderManager::GetShaderType(const char* type)
{
    if (std::strcmp(type, "pixel") == 0) return SHADER_TYPE::SHADER_PIXEL;
    else if (std::strcmp(type, "vertex") == 0) return SHADER_TYPE::SHADER_VERTEX;

    return SHADER_TYPE::SHADER_NONE;
}

std::string Charon::ShaderManager::GetShaderCompletePath(const std::string& fileName)
{
    char newPath[MAX_PATH] = "";
    std::stringstream ss;
    ss << "..\\Game\\Shaders\\" << fileName;

    PathCombine(newPath, GameEngine::GetInstance()->GetSystem<FileSystemManager>()->GetExecutableDirectory().c_str(), ss.str().c_str());

    return std::string(newPath);
}

ShaderDef* Charon::ShaderManager::GetShader(const std::string& shaderName)
{
    for (auto& shader : shaders)
    {
        if (shader->GetName() == shaderName)
            return shader.get();
    }

    return nullptr;
}