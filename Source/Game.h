#ifndef GAME_H
#define GAME_H

#include <string>
#include <thread>
#include <Engine/OGLGame.h>

#include "../Projects/AngryBirds/Mouse.h"
#include "../Projects/AngryBirds/Scene.h"
#include "../Projects/AngryBirds/Level.h"
#include "../Projects/AngryBirds/Menu.h"
#include "../Projects/AngryBirds/Physics.h"
#include "../Projects/AngryBirds/Audio.h"

#include "GameObject.h"

/**
*  An OpenGL Game based on ASGE.
*/
class AngryBirdsGame :
	public ASGE::OGLGame
{
public:
	AngryBirdsGame() = default;
	~AngryBirdsGame();
	virtual bool init() override;
	bool isFrapsRunning();																				// Is Fraps running?

	bool diag_mode = false;																				// Enable or disable the diagnostic messages shown on the upper right of the screen.

private:
	void keyHandler(const ASGE::SharedEventData data);													// The key handler function (handles key presses).
	void clickHandler(const ASGE::SharedEventData data);												// Handles mouse clicks.
	void moveHandler(const ASGE::SharedEventData data);													// Handles mouse movement.
	void scrollHandler(const ASGE::SharedEventData data);												// Handles mouse scroll.
	void setupResolution();																				// Sets up game resolution.
	void loadGame();																					// Load all core game functions, executed once the engine has loaded.
	void showLoadingSprite();																			// Shows the game loading screen.

	virtual void update(const ASGE::GameTime&) override;												// Core game update function, executed on every tick.
	virtual void render(const ASGE::GameTime&) override;												// Core game render function, called after the game update function.
	bool setWindowTitle(std::string desired_window_title);												// Sets the title of the window.
	std::string centreString(std::string input, int width);												// Pad a string with spaces to fill a defined width.

	int  key_callback_id = -1;																			// Keyboard key callback ID. Used for event handling.
	int  mouse_callback_id = -1;																		// Mouse button callback ID. Used for event handling.
	int  mouse_move_callback_id = -1;																	// Mouse move callback ID. Used for event handling.
	int  scroll_callback_id = -1;																		// Mouse scroll callback ID. Used for event handling.

	int score = 0;																							// The game score.
	int font_id = 0;																						// The current font ID that is being used.

	std::unique_ptr<Mouse> mouse = nullptr;																// Pointer to the Mouse class.
	std::unique_ptr<Scene> scene = nullptr;																// Pointer to the Scene class.
	std::unique_ptr<Menu> menu = nullptr;																// Pointer to the Menu class.
	std::unique_ptr<Level> level = nullptr;																// Pointer to the Level class.
	std::unique_ptr<Physics> physics = nullptr;															// Pointer to the Physics class.
	ASGE::Font* font = nullptr;																			// Pointer to the ASGE Font class.
	std::unique_ptr<Audio> audio = nullptr;																// Pointer to the Audio class.
	//std::unique_ptr<Profiles> profiles = nullptr;														// Pointer to the profile system.

	std::unique_ptr<GameObject> menu_fade_text = nullptr;																// Pointer for the game name on the menu title screen.
	std::unique_ptr<GameObject> game_loading_sprite_stage_one = nullptr;												// Pointer for the stage 1 loader image.
	std::unique_ptr<GameObject> game_loading_sprite_stage_two = nullptr;												// Pointer for the stage 2 loader image.
	std::unique_ptr<GameObject> game_loading_sprite_stage_three = nullptr;												// Pointer for the stage 3 loader image.
	std::unique_ptr<GameObject> game_loading_sprite_stage_four = nullptr;												// Pointer for the stage 4 loader image.
	std::unique_ptr<GameObject> game_loading_sprite_stage_five = nullptr;												// Pointer for the stage 5 loader image.
	std::unique_ptr<GameObject> chapters_text = nullptr;																// Text containing the word "CHAPTERS".
	std::unique_ptr<GameObject> fraps_warning_msg = nullptr;

	double time_since_last_check = 0;																		// Time since last check, used for the menu title screen game name.
	double ani_dt = 0;																				// The animation delta time. Starts from when the game has fully loaded.
	double loading_complete_dt = 0;																			// Aids in calculating the animation delta time.
	double level_x_pos = 0;;																					// Stores the current panning x position of the level.
	double level_y_pos = 0;																			// Stores the current panning y position of the level.
	double level_tile_height = 0;																			// Stores the height of level tiles.
	double game_menu_intro_text_fade_time = 0;																// Stores the current fade-in state of the menu title screen's game name.
	double game_menu_intro_fade_in_interval = 1000;														// The duration of the fade-in animation for the menu title screen's game name.
	double game_menu_buttons_fade_in_interval = 500;													// As above, but for the menu's buttons.
	double game_menu_buttons_opacity = 0;																	// Stores the current fade-in opacity of the game menu buttons.

	bool initial_load = false;																			// Stores if the game engine has fully loaded.
	bool fraps_detected = false;
	bool has_started_object_load = false;																// Has the game started loading?
	bool loading_complete = false;																		// Has the game finished loading?

	int loading_stage = 1;																				// Stores the current loading stage, used for rendering the correct loading stage screen.

	std::string ls_curr_world;																			// Stores the current level selection world.
};

#endif