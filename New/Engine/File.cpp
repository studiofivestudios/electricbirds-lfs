/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	File (Code File).
*/

#include "File.h"

Charon::File::File(const char* _fileName, const bool &keepOpen) : fileName(_fileName) {
	Open();

	if (!keepOpen)
		fileHandle.close();
}

bool Charon::File::GetIsClosedOnDisk() {
	return !fileHandle.is_open();
}

void Charon::File::Open(const bool& appendMode) {
	if (!GetIsClosedOnDisk()) {
		std::cout << "Charon : WARN: Filesystem attempted to open a file that is already open.";
		return;
	}

	if (appendMode)
		fileHandle.open(fileName, std::ios::app | std::ios::out | std::ios::in);
	else
		fileHandle.open(fileName, std::ios::out | std::ios::in);
	
	std::string line, text;
	while (std::getline(fileHandle, line))
	{
		text += line;
	}

	fileContents = text;
}

void Charon::File::Reload(const bool &appendMode = false) {
	Close();
	Open(appendMode);
}

void Charon::File::Write(const unsigned char* contents, const bool& append) {
	if (append)
		Reload(true);

	fileHandle << contents;
}
