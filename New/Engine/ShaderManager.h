/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Shader Manager (Header).
*/

#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H

#include <string>
#include <sstream>
#include <memory>
#include <map>

#include "EngineSystem.h"
#include "FileSystem.h"
#include "Enums.h"
#include "XMLParser.h"
#include "ShaderDef.h"

namespace Charon {
	class ShaderManager : public EngineSystem
	{
	public:
		ShaderManager() = default;
		~ShaderManager() = default;

		void Initialise() override;

		ShaderDef* GetShader(const std::string& shaderName);

		std::vector<std::shared_ptr<ShaderDef>> GetAllShaders() { return shaders; };
	private:
		SHADER_TYPE GetShaderType(const char* type);
		std::string GetShaderCompletePath(const std::string &fileName);

		const char* shaderDirectory = "..\\Game\\Shaders";
		std::filesystem::path tmpPath;

		std::vector<std::shared_ptr<ShaderDef>> shaders;
	};
}
#endif