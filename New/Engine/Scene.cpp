#include "Scene.h"

std::vector<Charon::Vertex> Charon::Scene::GetAllModelVerts()
{
    std::vector<Vertex> verts;

    for (auto& ent : entities)
    {
        if (typeid(ent.get()) == typeid(GameInterface::ThreeDModel*))
        {
            for (auto& modelVert : dynamic_cast<GameInterface::ThreeDModel*>(ent.get())->GetModel()->GetModelVertices())
                verts.push_back(modelVert);
        }  
    }

    return verts;
}

void Charon::Scene::RemoveEntityByName(const std::string& name)
{
    EntityBase* entity = FindEntityByName(name);

    if (entity)
    {
        entity->SetActive(false);
        entity->OnDisabled();

        // C++20, see https://docs.microsoft.com/en-us/visualstudio/productinfo/vs-roadmap for feature support.
        std::erase(entities, FindEntityByNamePtr(name));
    }
}

void Charon::Scene::RemoveEntityById(const unsigned int& id)
{
    EntityBase* entity = FindEntityById(id);

    if (entity)
    {
        entity->SetActive(false);
        entity->OnDisabled();

        // C++20, see https://docs.microsoft.com/en-us/visualstudio/productinfo/vs-roadmap for feature support.
        std::erase(entities, FindEntityByNamePtr(name));
    }
}

void Charon::Scene::DisableAllEntities()
{
    for (auto& entity : entities)
    {
        prevEntityStatesOnDisable.insert(std::pair<unsigned int, bool>(entity.get()->GetId(), entity.get()->GetState()));
        entity.get()->SetActive(false);
        entity.get()->OnDisabled();
    }
}

void Charon::Scene::EnableAllEntities(const bool& restore)
{
    EntityBase* entity;
    
    for (auto& entId : prevEntityStatesOnDisable)
    {
        entity = FindEntityById(entId.first);

        if (entity)
        {
            if (restore)
                entity->SetActive(entId.second);
            else
                entity->SetActive(true);
        }
    }
    
    prevEntityStatesOnDisable.clear();

}

void Charon::Scene::Activate()
{
    active = true;

    for (auto& ent : entities)
        ent->OnEnabled();

    for (auto& ent : entities)
        ent->OnAwake();

    for (auto& ent : entities)
        ent->OnStart();
}

void Charon::Scene::Deactivate()
{
    active = false;

    for (auto& ent : entities)
        ent->OnDisabled();
}

void Charon::Scene::Destroy()
{
    Deactivate();

    for (auto& ent : entities)
        ent->OnDestroy();
}

GameInterface::EntityBase* Charon::Scene::FindEntityByName(const std::string& name)
{
    // These shouldn't be dangling because it just returns the main shared_ptr from the array.
    return FindEntityByNamePtr(name).get();
}

GameInterface::EntityBase* Charon::Scene::FindEntityById(const unsigned int& id)
{
    return FindEntityByIdPtr(id).get();
}

std::shared_ptr<GameInterface::EntityBase> Charon::Scene::FindEntityByNamePtr(const std::string& name)
{
    for (auto& ent : entities)
    {
        if (ent.get()->GetName() == name)
            return ent;
    }

    return nullptr;
}

std::shared_ptr<GameInterface::EntityBase> Charon::Scene::FindEntityByIdPtr(const unsigned int& id)
{
    for (auto& ent : entities)
    {
        if (ent.get()->GetId() == id)
            return ent;
    }

    return nullptr;
}
