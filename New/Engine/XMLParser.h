/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	XML Parser Interface (Header).
*/

#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <string>
#include <fstream>
#include <iostream>

#include "ExtenLibs/rapidxml-1.13/rapidxml.hpp"
#include "ExtenLibs/rapidxml-1.13/rapidxml_print.hpp"

#include "EngineSystem.h"
#include "GameEngine.h"
#include "FileSystem.h"

namespace Charon
{
    class XMLParser : public EngineSystem
    {
    public:
        XMLParser() = default;
        ~XMLParser() = default;

        std::shared_ptr<rapidxml::xml_document<>> ParseXML(const std::string& path);
    };
}
#endif