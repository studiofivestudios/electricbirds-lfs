/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	ThreeDModel (Header).
*/

#ifndef SPRITEDEF_H
#define SPRITEDEF_H

#include "EntityBase.h"
#include "Vector3.h"
#include "Model.h"

namespace GameInterface {
	class ThreeDModel : public EntityBase
	{
	public:
		enum class Operation
		{
			Set,
			Add,
			Subtract,
			Multiply,
			Divide
		};

		ThreeDModel(const unsigned int& _entityId, const std::string& _name, EntityBase* parent = nullptr) : EntityBase(_entityId, _name, parent) { };
		~ThreeDModel() { Cleanup(); OnDestroy(); };

		void SetModel(std::shared_ptr<Charon::Model> _model);
		void SetPosition(const Charon::Mathematics::Vector3& pos, const Operation &operation = Operation::Set);
		void SetRotation(const Charon::Mathematics::Vector3& rotation, const Operation& operation = Operation::Set);
		void SetScale(const Charon::Mathematics::Vector3& scale, const Operation& operation = Operation::Set);

		Charon::Mathematics::Vector3 GetPosition() { return model.get()->GetPosition(); };
		Charon::Mathematics::Vector3 GetRotation() { return model.get()->GetRotation(); };

		Charon::Model* GetModel() { return model.get(); }

	private:
		std::shared_ptr<Charon::Model> model;

		void Cleanup();
	};
}
#endif