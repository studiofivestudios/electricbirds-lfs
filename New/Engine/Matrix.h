/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Matrix (Header).
* 
*	Welcome Neo, the matrix is watching.
*
*	With help from:
*		What exactly does XMMatrixPerspective{L,R}H do? (Gamedev SE)- https://gamedev.stackexchange.com/questions/168067/what-exactly-does-xmmatrixperspectivel-rh-do
*		Matrix4x4.h- https://github.com/PardCode/CPP-3D-Game-Tutorial-Series/blob/AllTutorials/Tutorial14_GraphicsEngine_Camera/Matrix4x4.h
*/

#ifndef MATRIXDEF_H
#define MATRIXDEF_H

#include <DirectXMath.h>

#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Translate.h"

using namespace DirectX;
using namespace Charon::Mathematics;

namespace GameInterface {
	class Matrix
	{
	public:
		Matrix() { SetScale( { 1.0f, 1.0f, 1.0f } ); };
		~Matrix() = default;

		struct MatrixCameraStorage
		{
			Matrix* viewMatrix;
			Matrix* projectionMatrix;
		};

		DirectX::XMMATRIX GetMatrix(MatrixCameraStorage cameraMatricies = {}) { return ApplyMatrix(cameraMatricies); }

		XMMATRIX GetTranslation();
		XMVECTOR GetDeterminant();

		void SetOrthographic(const Vector2& widthHeight, const Vector2& nearFarPlane);
		void SetPerspective(const float& aspect, const Vector2& nearFarPlaneZ, const float& fieldOfView = 65.0f);
		void SetRotation(const Vector3& _rotation);
		void SetLookAtPosition(const Vector3& lookAtCoords);
		void SetPosition(const float& x, const float& y, const float& z);
		void SetScale(const Vector3& scale);
		void SetTranslation(const XMFLOAT3& translation);
		void SetIdentity();
		void SetMatrix(const DirectX::XMMATRIX& newMatrix) { matrix = newMatrix; }

		void InverseMatrix();

		void CreateViewMatrix();

		XMFLOAT3 GetPosition() { return position; }
		XMFLOAT3 GetRotation(const bool &inDeg = true) { return inDeg ? Translate::RadiansToDegreesXM(rotation) : rotation; }
		XMFLOAT3 GetScale() { return scaleUF; }

		XMMATRIX ApplyMatrix(MatrixCameraStorage cameraMatricies);
	private:
		XMMATRIX matrix { };
		XMMATRIX translation{ };
		XMMATRIX scale{ };

		XMFLOAT3 position = XMFLOAT3(0, 0, 0);
		XMFLOAT3 rotation = XMFLOAT3(0, 0, 0);
		XMFLOAT3 lookAt = XMFLOAT3(0, 0, 1);
		XMFLOAT3 scaleUF = XMFLOAT3(1, 1, 1);;

		XMVECTOR GetCrossProduct(const XMVECTOR vec[3]);
	};
};
#endif