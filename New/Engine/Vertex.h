/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Vertex Definition (Header).
*/

#ifndef VERTEXDEF_H
#define VERTEXDEF_H

#include <DirectXMath.h>

#include "Vector3.h"
#include "Colour.h"

namespace Charon {
	struct Vertex
	{
		Charon::Mathematics::Vector3 position;
		Colour colour;
	};
}
#endif