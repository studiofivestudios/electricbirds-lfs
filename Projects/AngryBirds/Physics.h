/*
	This class is responsible for all physics calculations

	Created : 16th February 2018 @ 18:43
	Updated: June 2021
	Author : Alexander Stopher
*/

#ifndef PHYSICS_H
#define PHYSICS_H

#include "GameObject.h"
#include "Mathematics.h"

class Physics
{
public:
	Physics();
	~Physics();

	bool initialize();

	/*
		This controls the resolution of the bezier curve (how many positions per curve are sampled and stored).
		Default is 200 (0.5% of the curve for each sample).
	*/
	static const int BEZIER_RESOLUTION = 200;

	struct physical_body
	{
		float weight = 0.0f;
		double time_stepping = 0;
	};

	double delta_time = 0;

	Mathematics::pos bezier_trajectory[BEZIER_RESOLUTION];

	//This version of the function is for slingshots ONLY due to the automated control and end points.
	void calculateBezierTrajectory(GameObject* sprite, Mathematics::pos mouse_pos, Mathematics::pos floor_pos, Mathematics::pos a, double game_height, double game_width);
	void calculateBezierTrajectory(GameObject* sprite, Mathematics::pos control_point, Mathematics::pos a, Mathematics::pos b, double resolution);

	Mathematics::pos calculateBezierEndPoint(Mathematics::pos control_point, Mathematics::pos a, double stage_floor_offset, Mathematics::pos mouse_pos);
	Mathematics::pos calculateGravitySpeedInterpolation(GameObject& sprite, bool is_moving_upwards, float pos_x, float pos_y, int vec_x, int vec_y);
	float calculateRotationsPerUpdate(GameObject& sprite);
	bool hasLoaded();

private:
	bool loaded = false;
};

#endif