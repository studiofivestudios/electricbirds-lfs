/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Vertex Factory (Code File).
*
*	Creates a Model object from the given dimensions and screen coordinates (a Model is simply a single shape).
*/

#include "VertexFactory.h"

#include "GameEngine.h"

// Draw a rectangle at x position with y width and height (2D).
void Charon::VertexFactory::Create(const Charon::Mathematics::Vector2& wh, const Charon::RENDER_OBJECT_TYPE& objectType, GameInterface::ThreeDModel* entity, const Colour& colour)
{
	std::vector<Vertex> tmpVerts;

	tmpVerts.push_back(Vertex{ Charon::Mathematics::Vector3{ 0.0f, 0.0f, 0.0f}, colour });
	tmpVerts.push_back(Vertex{ Charon::Mathematics::Vector3{ wh.x, 0.0f, 0.0f}, colour });
	tmpVerts.push_back(Vertex{ Charon::Mathematics::Vector3{ 0.0f, wh.y, 0.0f}, colour });
	tmpVerts.push_back(Vertex{ Charon::Mathematics::Vector3{wh.x, wh.y, 0.0f}, colour });

	std::shared_ptr<Charon::Model> model = std::make_shared<Charon::Model>(tmpVerts);

	entity->SetModel(model);
}

// Draw an object with multiple polygons (3D).
void Charon::VertexFactory::Create(const std::vector<Charon::Mathematics::Vector3>& vertPositions, const Charon::RENDER_OBJECT_TYPE& objectType, GameInterface::ThreeDModel* entity)
{
	std::vector<Vertex> tmpVerts;

	for (auto vertPos : vertPositions)
		tmpVerts.push_back(Vertex{ Charon::Mathematics::Vector3 { vertPos.x, vertPos.y, vertPos.z }, Colour{ 255,255,0,255 } });

	entity->SetModel(std::make_shared<Charon::Model>(tmpVerts));
}