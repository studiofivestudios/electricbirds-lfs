/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Vertex Factory (Header).
* 
*	Creates a Model object from the given dimensions and screen coordinates (a Model is simply a single shape).
*/

#ifndef VERTEXFACTORY_H
#define VERTEXFACTORY_H

#include <vector>

#include "Vertex.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Enums.h"
#include "ThreeDModel.h"
#include "Scene.h"

namespace Charon
{
	class VertexFactory
	{
	public:
		VertexFactory() = default;
		~VertexFactory() = default;

		void Create(const Charon::Mathematics::Vector2& wh, const Charon::RENDER_OBJECT_TYPE& objectType, GameInterface::ThreeDModel* entity, const Colour &colour = Colour{ 0.0f, 1.0f, 0.0f, 1.0f });
		void Create(const std::vector<Charon::Mathematics::Vector3> &vertPositions, const Charon::RENDER_OBJECT_TYPE& objectType, GameInterface::ThreeDModel* entity);
	};
}

#endif