/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Shader Definition (Code File).
*/

#include "ShaderDef.h"

Charon::ShaderDef* Charon::ShaderDef::Load(const std::string& filePath, const std::string& _shaderName, const SHADER_TYPE& _shaderType)
{
	shaderType = _shaderType;
	HRESULT loadResult = D3DCompileFromFile(std::wstring(filePath.begin(), filePath.end()).c_str(), NULL, NULL, entryPoint.c_str(),
		shaderVersion.c_str(), NULL, NULL, &compiledShaderObject, &errorBlob);
	
	if (loadResult == S_OK)
	{
		std::cout << "Charon : Shader loaded: " << shaderName << ", of type \"" << (shaderType == SHADER_TYPE::SHADER_VERTEX ? "vertex" : "pixel") << "\".\n";
		return this;
	}
	else
		std::cout << "Charon : Shader load failed for " << shaderName << ", D3DCompileFromFile error is: \"" << static_cast<char*>(errorBlob->GetBufferPointer()) << "\".\n";

	return nullptr;
}