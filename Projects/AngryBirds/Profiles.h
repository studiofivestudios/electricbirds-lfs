/*
	This class is responsible for all user profile actions, such as creation, saving and updates.

	Created: 16th April 2018 at 19:26
	Author: Alexander Stopher
*/

#include <string>

class Level;

#pragma once
class Profiles
{
public:
	Profiles();
	~Profiles();

	struct profile 
	{
		std::string name;																						// Name given by the profile's creator to the profile.
		std::string f_name;																						// File name of the profile storage file.
		bool is_current;																						// Is this the current profile?
	};

	struct level_data
	{
		std::string world;
		std::string name;
		int score;
		int high_score;
		bool locked;
	};

	bool initialize(Level *level_sys);																							// Initialize the game profile system (also loads all profiles automatically).

	std::string getNameOfActiveProfile();																		// Retrieve the name of the current profile
	bool addProfile(profile profile_obj);																		// Add a profile to the system.
	bool deleteProfile(profile profile_obj, std::string backwards_name);										// Deletes a profile.
	bool changeProfileName(profile profile_obj, std::string name);												// Overwrite the name of a profile.
	bool createProfile(profile profile_obj);																	// Create a profile.
	bool loadProfile(profile profile_obj);																		// Load a specific profile.
	int getAmountOfLoadedProfiles();																			// Retrieve the amount of profiles that have been loaded.

	void setActiveProfile(profile profile_obj);																	// Save the given profile.
	
private:
	static const int MAX_PROFILE_COUNT = 5;																		// The maximum amount of profiles that can be created.
	int profile_loaded_count = 0;

	Level* level = nullptr;																						// Pointer to the level system.

	profile* current;																							// Pointer to the instance of the current profile.
	profile all_profiles[MAX_PROFILE_COUNT] = { };																// Array containing all profiles.

	bool saveProfileChanges(profile profile_obj);																// Save changes to a profile, along with game progress.
	bool restoreGameProgress(profile profile_obj);																// Restore game progress.
	bool loadProfileFromFile(profile profile_obj);																// Load the profile's file and interpolate.
	
	void loadAllProfiles();
};

