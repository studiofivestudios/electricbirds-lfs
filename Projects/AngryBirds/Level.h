/*
	This class is responsible for the loading of levels.

	Created : 21st February 2018 @ 15:10
	Updated: June 2021
	Author : Alexander Stopher
*/

#ifndef LEVEL_H
#define LEVEL_H

#include <Engine/Sprite.h>
#include <GameObject.h>
#include "Game.h"
#include "Profiles.h"

class Level
{
public:
	Level();
	~Level();

	static const int CHAPTER_COUNT = 8;																						// The number of chapters.

	struct level_item																										// A structure to store parameters for this level object.
	{
		GameObject* game_object = new GameObject();																			// GameObject component for this level object.
		GameObject* despawn_game_object = new GameObject();																	// GameObject component for this level object's despawning process.
		double x_pos;																										// X position of this GameObject.
		double y_pos;																										// Y position of this GameObject.
		std::string texture_file_location_1;																				// Texture file path for this GameObject.
		std::string type;																									// The type of GameObject.
		std::string subtype;																								// The subtype of GameObject.
		double weight;																										// Weight, in KG, of the GameObject. Used for physics calculations.
		double score = 0;																									// Associated score for this GameObject.
	};

	struct level
	{																														// A structure to store parameters for this level.
		GameObject* tile = nullptr;																							// A GameObject component for the level selection tile.
		GameObject* completed_icon = nullptr;
		bool is_locked;																										// Is the level currently locked?
		bool is_current;																									// Is this the level that we're currently running?
		std::string name;																									// Friendly name of the level.
		std::string world;																									// Friendly name of the world.
		bool is_completed = false;																							// Is the level complete?
		bool is_completed_this_round = false;																				// Has the level been completed this round of play?
		double score = 0;																									// Associated score achieved in the level, if any.
		bool game_over = false;																								// Has the level been completed in this session?
		int num_of_objects = 0;																								// How many level objects do we have in this level?
		level_item objects[64] = {};																						// Container holding all objects of this level.
	};

	void initializeLevel(int level_num, bool initialInit);																	// Initialize the level.
	bool initialize(std::unique_ptr<ASGE::Renderer>* renderer, Scene& scene, Mouse& mouse, Physics& physics_p, Menu& menu, Audio& audio);		// Initialize the level engine.
	int num_of_levels = 0;																									// How many levels exist in total?
	static const int MAX_NUM_OF_LEVELS = 256;																				// The maximum number of levels supported.

	GameObject* cannon = nullptr;																							// The cannon object present in every level.
	GameObject* cannon_leg_front = nullptr;
	GameObject* level_failed = nullptr;																						// The Dark Souls-type "level failed" image.
	GameObject* flight_path_markers[50];																					// An array of GameObjects for the flight path markers.

	level level_storage[MAX_NUM_OF_LEVELS] = {};																							// An array of levels.

	bool getLevels();																										// Read all levels from file and populate arrays.
	bool readyLevelSelection();																								// Create the level selection menu.
	Level::level* getCurrentLevel();																							// Get the level that is currently active.
	int getLevelNum(level* level_obj);																						// As above, but gets the number of the level in the level storage array.
	void renderLevel();																										// Renders the level.
	void processInput();																									// Processes all level input.
	bool isCurrentLevelLast();																								// Are there any further levels to be played?
	void onUpdate(double dt_sec);																							// Level update function.

	std::string getCurrentWorld();																							// Get the name of the current world.
	int getNumOfCompletedLevels();																							// Get the amount of levels in the world that have been completed.
	int getNumOfCompletedLevelsForWorld(int world);																			// Retrieves the number of completed levels for the specified world.
	int getNumOfLevelsInWorld(std::string world);																			// Get the number of levels in the world.
	int getNumOfLevelsInWorld(int world);
	double getCurrentScore();																								// Get the current score.
	bool hasLoaded();																										// Has the level engine been initialized?
	void setPaused(bool paused);																							// Pause or unpause the game.
	bool isGamePaused();

	void doAnimations(double dt_sec);																						// Perform an update to any animations.

	float getCannonCharacterVelocity();																						// Get the current velocity of the character that was fired.

	double cannon_angle = 0;																								// Angle in radians of the cannon.

	Profiles::level_data level_profile_data[MAX_NUM_OF_LEVELS];																// Game progress data loaded from file.

	void loadChapterNames(std::unique_ptr<ASGE::Renderer>* renderer);														// Loads chapter name images to the array.
	GameObject* getChapterName(std::string name);																			// Retrieve chapter number image.
	int getChapterId(GameObject* chapter);																					// Retrieve the chapter's numerical ID.
	std::string getChapterIdAsString(GameObject* chapter);
	GameObject* getChapterName(int chapter_i);

	GameObject* chapterNames[CHAPTER_COUNT - 1];																			// Array containing chapter names.
	GameObject chapter_name_closest_to_top;

private:
	std::unique_ptr<ASGE::Renderer>* renderer = nullptr;																	// Pointer to the main engine renderer.

	bool hasUnlockedLevel(level* level_file);																				// Has the player unlocked the specified level?
	void readyCannon();																										// Apply positioning and angle corrections to the cannon.
	void rotateAroundSprite(double degrees, GameObject* sprite, double x_pos, double y_pos);								// Rotate an object around another.
	void setGameOver(bool is_game_over);																					// Sets the game over state.

	void showFlightPath(GameObject* object);																				// Show the current flight path.
	void hideFlightPath(GameObject* object);																				// Hide the current flight path
	void doObjectSticky(GameObject* cannon_character);																		// Check for object collisions.

	bool cannon_is_active = false;																							// is the player currently pulling back to fire?
	bool is_character_in_cannon;																							// Is there a character in the cannon?

	double level_timer = 0.0;																								// Stores time since the start of the level, in delta time.
	double level_over_timer = 0.0;																							// Stores the time when the level was over.

	GameObject* cannon_character = nullptr;																					// Pointer to the character within the cannon.
	Scene* scene = nullptr;																									// Pointer to the Scene class.
	Mouse* mouse = nullptr;																									// Pointer to the Mouse class.
	Physics* physics = nullptr;																								// Pointer to the Physics class.
	Menu* menu = nullptr;																									// Pointer to the menu class.
	Audio* audio = nullptr;																									// Pointer to the audio class.

	level* current_level;																									// The current level.
	bool game_over = false;																									// Stores if the level has ended.
	bool paused = false;

	bool loaded = false;																									// Has the level system loaded?
	bool hasStartedInit = false;																							// Have we started to load the level system?
};

#endif