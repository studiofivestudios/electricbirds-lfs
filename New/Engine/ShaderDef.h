/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Shader Definition (Header).
*/

#ifndef SHADERDEF_H
#define SHADERDEF_H

#include <iostream>
#include <string>
#include <sstream>
#include <d3d11.h>
#include <d3dcommon.h>
#include <d3dcompiler.h>

#include "Enums.h"

namespace Charon {
	class ShaderDef
	{
	public:
		ShaderDef(const char* _shaderName, const char* _entryPoint, const char* _shaderVersion) : shaderName(_shaderName), entryPoint(_entryPoint), shaderVersion(_shaderVersion) { };
		~ShaderDef() = default;

		Charon::ShaderDef* Load(const std::string& filePath, const std::string& _shaderName, const SHADER_TYPE& shaderType);

		SHADER_TYPE GetShaderType() { return shaderType; }
		ID3D10Blob* GetCompiledShader() { return compiledShaderObject; }
		ID3D10Blob* GetShaderCompilerErrorPtr() { return errorBlob; }
		ID3D11VertexShader* GetVertexShader() { return vertexShader; }
		ID3D11PixelShader* GetPixelShader() { return pixelShader; }
		ID3D11VertexShader** GetVertexShaderAddr() { return &vertexShader; }
		ID3D11PixelShader** GetPixelShaderAddr() { return &pixelShader; }

		std::string GetName() { return shaderName; }
	private:
		ID3D10Blob* compiledShaderObject = nullptr;
		ID3D10Blob* errorBlob = nullptr;

		ID3D11VertexShader* vertexShader = nullptr;
		ID3D11PixelShader* pixelShader = nullptr;

		SHADER_TYPE shaderType = SHADER_TYPE::SHADER_NONE;

		std::string shaderName;
		std::string entryPoint;
		std::string shaderVersion;
	};
}
#endif