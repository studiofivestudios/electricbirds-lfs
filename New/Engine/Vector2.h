/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	2D Vector (Header).
*/

#ifndef VECTOR2_H
#define VECTOR2_H

namespace Charon::Mathematics {
	struct Vector2
	{
		float x = 0.0f;
		float y = 0.0f;

		Vector2 operator *(float num)
		{
			return Vector2{ x * num, y * num };
		}

		Vector2 operator -(float num)
		{
			return Vector2{ x - num, y - num };
		}

		Vector2 operator +(Vector2 vec)
		{
			return Vector2{ x + vec.x, y + vec.y };
		}
	};

	static Vector2 Vector2Left		= Vector2{	-1,	0	};
	static Vector2 Vector2Right		= Vector2{	1,	0	};
	static Vector2 Vector2Up		= Vector2{	0,	1	};
	static Vector2 Vector2Down		= Vector2{	0,	-1	};
}
#endif