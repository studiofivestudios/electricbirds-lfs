/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	File System Manager (Code File)
*/

#include "FileSystem.h"

Charon::File* Charon::FileSystemManager::LoadFile(const char* fileName, const bool &keepOpen, const bool &relativeToGame) {
	char newPath[MAX_PATH] = "";
	PathCombine(newPath, GetExecutableDirectory().c_str(), fileName);

	if (!IsFileAlreadyLoaded(newPath)) {
		files.push_back(std::make_unique<File>(newPath));

		return files[files.size() - 1].get();
	}

	return nullptr;
}

char* Charon::FileSystemManager::LoadAndGetContents(const char* fileName, const bool& keepOpen, const bool& relativeToGame) {
	File* file = LoadFile(fileName, keepOpen, relativeToGame);

	if (file != nullptr)
	{
		const char *_fileContents = file->GetContents();
		return const_cast<char*>(_fileContents);
	}

	return nullptr;
}

bool Charon::FileSystemManager::WriteFile(const char* fileName, const unsigned char* fileContents)
{
	return false;
}

bool Charon::FileSystemManager::IsFileAlreadyLoaded(const char* fileName) {
	for (std::unique_ptr<File> &file : files) {
		if (file.get()->GetName() == fileName)
			return true;
	}

	return false;
}

std::vector<std::string> Charon::FileSystemManager::EnumerateDirectory(const char* directory, const bool &relativeToGame)
{
	std::vector<std::string> results;
	char newPath[MAX_PATH] = "";

	if (relativeToGame)
		PathCombine(newPath, GetExecutableDirectory().c_str(), directory);

	if (std::filesystem::exists(newPath))
	{
		for (const auto& entry : std::filesystem::directory_iterator(newPath))
			results.push_back(entry.path().string());
	}

	return results;
}

//https://gist.github.com/karolisjan/f9b8ac3ae2d41ec0ce70f2feac6bdfaf
std::string Charon::FileSystemManager::GetExecutableDirectory()
{
	char buffer[MAX_PATH];
	GetModuleFileNameA(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");

	return std::string(buffer).substr(0, pos);
}