/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	RayIntersection (Header).
*/

#ifndef RAYINTERSECTION_H
#define RAYINTERSECTION_H

#include "EntityBase.h"
#include "Vector2.h"

namespace GameInterface
{
	class RayIntersection
	{
	public:
		static bool Hit(EntityBase* entity, Charon::Mathematics::Vector2 mouseCoordinates);
	};
}
#endif // RAYINTERSECTION_H
