/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Matrix (Implementation).
*/

#include "Matrix.h"

XMMATRIX GameInterface::Matrix::GetTranslation()
{
	return translation;
}

XMVECTOR GameInterface::Matrix::GetDeterminant()
{
	return XMMatrixDeterminant(matrix);
}

void GameInterface::Matrix::SetOrthographic(const Vector2& widthHeight, const Vector2& nearFarPlane)
{
	matrix = XMMatrixOrthographicLH(widthHeight.x, widthHeight.y, nearFarPlane.x, nearFarPlane.y);
}

void GameInterface::Matrix::SetPerspective(const float& aspect, const Vector2& nearFarPlaneZ, const float& fieldOfView)
{
	matrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(fieldOfView), aspect, nearFarPlaneZ.x, nearFarPlaneZ.y);
	//matrix = XMMatrixPerspectiveFovLH(0.4f * 3.14f, (float)1024 / 768, 1.0f, 1000.0f);
}

XMMATRIX GameInterface::Matrix::ApplyMatrix(MatrixCameraStorage cameraMatricies)
{
	// Tried to pass just a Camera object here, but got caught in a header conflict so easier to just have an optional
	//		struct that contains the required view and projection matricies instead.
	if (cameraMatricies.viewMatrix != nullptr && cameraMatricies.projectionMatrix != nullptr)
	{
		
		// SCALE NOT BEING APPLIED HERE!
		matrix = XMMatrixTranspose(
			(XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z) *
				scale *
				XMMatrixTranslation(position.x, position.y, position.z)) *
			cameraMatricies.viewMatrix->GetMatrix() * cameraMatricies.projectionMatrix->GetMatrix()
		);
	}
	
	return matrix;
}

void GameInterface::Matrix::SetRotation(const Vector3& _rotation)
{
	rotation = Translate::DegreesToRadiansXM(XMFLOAT3(_rotation.x, _rotation.y, _rotation.z));
}

void GameInterface::Matrix::SetLookAtPosition(const Vector3& lookAtCoords)
{
	lookAt = XMFLOAT3(lookAtCoords.x, lookAtCoords.y, lookAtCoords.z);
}

void GameInterface::Matrix::SetPosition(const float& x, const float& y, const float& z)
{
	position = XMFLOAT3(x, y, z);
	SetTranslation(position);
}

void GameInterface::Matrix::SetScale(const Vector3& _scale)
{
	scaleUF = XMFLOAT3(scaleUF.x, scaleUF.y, scaleUF.z);
	scale = XMMatrixScaling(_scale.x, _scale.y, _scale.z);
}

void GameInterface::Matrix::SetTranslation(const XMFLOAT3& _translation)
{
	translation = XMMatrixTranslation(_translation.x, _translation.y, _translation.z);
}

void GameInterface::Matrix::SetIdentity()
{
	matrix.m[0][0] = 1.0f;
	matrix.m[1][1] = 1.0f;
	matrix.m[2][2] = 1.0f;
	matrix.m[3][3] = 1.0f;
}

void GameInterface::Matrix::InverseMatrix()
{
	XMVECTOR det = GetDeterminant();
	matrix = XMMatrixInverse(&det, matrix);
}

void GameInterface::Matrix::CreateViewMatrix()
{
	XMFLOAT3 up = XMFLOAT3(0, 0, 0), _lookAt = XMFLOAT3(0, 0, 0);
	XMVECTOR upVec, positionVec, lookAtVec;
	XMMATRIX rotationMatrix;

	up.x = Vector3Up.x;
	up.y = Vector3Up.y;
	up.z = Vector3Up.z;
	_lookAt.x = lookAt.x;
	_lookAt.y = lookAt.y;
	_lookAt.z = lookAt.z;

	upVec = DirectX::XMLoadFloat3(&up);
	positionVec = XMLoadFloat3(&position);
	lookAtVec = XMLoadFloat3(&_lookAt);

	rotationMatrix = XMMatrixRotationRollPitchYaw(rotation.x, rotation.y, rotation.z);

	lookAtVec = XMVector3TransformCoord(lookAtVec, rotationMatrix);
	upVec = XMVector3TransformCoord(upVec, rotationMatrix);
	lookAtVec = XMVectorAdd(positionVec, lookAtVec);

	matrix = XMMatrixLookAtLH(positionVec, lookAtVec, upVec);
}

XMVECTOR GameInterface::Matrix::GetCrossProduct(const XMVECTOR vec[3])
{
	return XMVector4Cross(vec[0], vec[1], vec[2]);
}