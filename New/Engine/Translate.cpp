/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Mathematics Translation Utilities (Code File).
*/

#include "Translate.h"

Charon::Mathematics::Vector3 Charon::Mathematics::Translate::DegreesToRadians(const Vector3& src)
{
    return Vector3{ DirectX::XMConvertToRadians(src.x), DirectX::XMConvertToRadians(src.y), DirectX::XMConvertToRadians(src.z) };
}

DirectX::XMFLOAT3 Charon::Mathematics::Translate::DegreesToRadiansXM(const DirectX::XMFLOAT3& src)
{
    return DirectX::XMFLOAT3( DirectX::XMConvertToRadians(src.x), DirectX::XMConvertToRadians(src.y), DirectX::XMConvertToRadians(src.z) );
}

Charon::Mathematics::Vector3 Charon::Mathematics::Translate::RadiansToDegrees(const Vector3& src)
{
    return Vector3{ DirectX::XMConvertToDegrees(src.x), DirectX::XMConvertToDegrees(src.y), DirectX::XMConvertToDegrees(src.z) };
}

DirectX::XMFLOAT3 Charon::Mathematics::Translate::RadiansToDegreesXM(const DirectX::XMFLOAT3& src)
{
    return DirectX::XMFLOAT3( DirectX::XMConvertToDegrees(src.x), DirectX::XMConvertToDegrees(src.y), DirectX::XMConvertToDegrees(src.z) );
}