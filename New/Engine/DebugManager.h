/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Debug Manager (Header)
*/

#ifndef DEBUGMANAGER_H
#define DEBUGMANAGER_H

#include <iostream>

#include "GameEngine.h"
#include "EngineSystem.h"

using namespace Charon;

namespace Charon {
	class DebugManager : public EngineSystem
	{
	public:
		DebugManager() = default;
		~DebugManager() = default;

		void Initialise() override;

		void OnQuit();
		void OnUpdate(const long long delta_t) override;
		
		float GetSystemSlowdownThreshold() { return systemSlowdownThreshold; };

	private:
		void SetDevelopmentConsoleTitle();

		long long devTitleUpdateTimer = 0;
		long long lastTime = 0;

		const float devTitleUpdateInterval = 1000; // In ms.

		float px, py = 0;

		/*
		* In ms, determines when the engine determines a possible engine slowdown is occurring.
		* If this threshold is exceeded, warning messages will appear in the developer console.
		*/
		const float systemSlowdownThreshold = 500;

		bool canUpdate = false;
	};
}

#endif