/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Main Game Header
*/

#include "Game.h"
#include "GameEngine.h"

using namespace Charon;
using namespace GameInterface;

Game::Game()
{
	loadingScene = GameEngine::GetInstance()->GetSystem<SceneManager>()->CreateNewScene("Loading", false);
	mainMenuScene = GameEngine::GetInstance()->GetSystem<SceneManager>()->CreateNewScene("MainMenu", false);
	mainGameScene = GameEngine::GetInstance()->GetSystem<SceneManager>()->CreateNewScene("Game", false);

	std::unique_ptr bootSceneManager = std::make_unique<BootScene>();
}