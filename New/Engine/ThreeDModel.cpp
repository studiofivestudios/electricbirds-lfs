/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	ThreeDModel (Code File).
*/

#include "ThreeDModel.h"
#include "GameEngine.h"

void GameInterface::ThreeDModel::SetModel(std::shared_ptr<Charon::Model> _model)
{
	if (model)
		Cleanup(); // Ensure that we remove the old model first! TODO: include textures in this!

	model = _model;
	GameEngine::GetInstance()->GetSystem<RenderManager>()->AddModelToBuffer(this);
}

void GameInterface::ThreeDModel::SetPosition(const Charon::Mathematics::Vector3& pos, const Operation& operation)
{
	Vector3 currentPos = model.get()->GetPosition();
	switch (operation)
	{
		case (Operation::Set):
			model.get()->SetPosition(pos.x, pos.y, pos.z);
			break;
		case (Operation::Add):
			model.get()->SetPosition(currentPos + pos);
			break;
		case (Operation::Subtract):
			model.get()->SetPosition(currentPos - pos);
			break;
		case (Operation::Multiply):
			model.get()->SetPosition(currentPos * pos);
			break;
		case (Operation::Divide):
			model.get()->SetPosition(currentPos / pos);
			break;
	}
}

void GameInterface::ThreeDModel::SetScale(const Charon::Mathematics::Vector3& scale, const Operation& operation)
{
	Vector3 currentScale = model.get()->GetScale();
	switch (operation)
	{
	case (Operation::Set):
		model.get()->SetScale(scale.x, scale.y, scale.z);
		break;
	case (Operation::Add):
		model.get()->SetScale(currentScale + scale);
		break;
	case (Operation::Subtract):
		model.get()->SetScale(currentScale - scale);
		break;
	case (Operation::Multiply):
		model.get()->SetScale(currentScale * scale);
		break;
	case (Operation::Divide):
		model.get()->SetScale(currentScale / scale);
		break;
	}
}

void GameInterface::ThreeDModel::SetRotation(const Charon::Mathematics::Vector3& rotation, const Operation& operation)
{
	Vector3 currentRotation = model.get()->GetRotation();
	switch (operation)
	{
	case (Operation::Set):
		model.get()->Rotate(rotation);
		break;
	case (Operation::Add):
		model.get()->Rotate(currentRotation + rotation);
		break;
	case (Operation::Subtract):
		model.get()->Rotate(currentRotation - rotation);
		break;
	case (Operation::Multiply):
		model.get()->Rotate(currentRotation * rotation);
		break;
	case (Operation::Divide):
		model.get()->Rotate(currentRotation / rotation);
		break;
	}
}

void GameInterface::ThreeDModel::Cleanup()
{
	GameEngine::GetInstance()->GetSystem<RenderManager>()->RemoveModelFromBuffer(this);
}