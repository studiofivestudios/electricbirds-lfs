/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Engine core (Header).
*/

#ifndef GAMEENGINE_H
#define GAMEENGINE_H

// TODO: Prune this list of includes!
#include <memory>
#include <string>
#include <vector>
#include <typeindex>
#include <atomic>
#include <variant>
#include <map>
#include <sstream>
#include <iostream>
#include <windows.h>
#include <typeinfo>
#include <wrl/client.h>
#include <windowsx.h>
#include <d3d11.h>

#include "EngineSystem.h" 
#include "RenderManager.h"
#include "DebugManager.h"
#include "Delegate.h"
#include "ShaderManager.h"
#include "SceneManager.h"
#include "FileSystem.h"

#include "Vector2.h"

using namespace Microsoft::WRL;
using namespace Charon::Mathematics;
using namespace GameInterface;

namespace Charon {
	class GameEngine
	{
	public:
		GameEngine() = default;
		~GameEngine() { std::cout << "Charon : Destroyed."; };
		void Initialise(const HWND& consoleHwnd, const char* _appName, const char* _gameBuildDate,
			const char* _gameBuildTime, Delegate *onEngineIntermediateLoadCallback = nullptr, Delegate* onEngineInitialisedCallback = nullptr);

		static GameEngine* GetInstance();

		void Quit(const int& exitCode, const std::string &msgBoxTitle, const std::string &msgBoxBody);
		void Quit(const int & exitCode);

		bool IsExiting();
		bool IsEngineReady() { return engineReady; }

		const char* GetEngineBuildDate() { return engineBuildDate; }
		const char* GetEngineBuildTime() { return engineBuildTime; }
		const char* GetGameBuildDate() { return gameBuildDate; }
		const char* GetGameBuildTime() { return gameBuildTime; }
		const char* GetApplicationName() { return appName; }

		HWND GetGameWindowHandle() { return gameWindowHwnd; }
		HWND GetDebugWindowHandle() { return debugConsoleWindowHwnd; }

		const char *GetEngineFileDirectory() { return engineFileDirectory; }

		long long GetDeltaTime() { std::lock_guard<std::mutex> lock(padlock); return lastDeltaTime; }
		float GetDeltaTimef() { std::lock_guard<std::mutex> lock(padlock); return static_cast<float>(lastDeltaTime); }
		long long GetCurrentTime() { std::lock_guard<std::mutex> lock(padlock); return currentTime; }
		long long GetAppRunTime() { std::lock_guard<std::mutex> lock(padlock); return (currentTime - appStartTime); }
		long long GetAppStartTime() { std::lock_guard<std::mutex> lock(padlock); return appStartTime; }

		Vector2 GetGameWindowDimensions() { std::lock_guard<std::mutex> lock(padlock); return gameWindowDimensions; }

		/* Get a specific game engine system, as defined by "T".
		*  Per https://en.cppreference.com/w/cpp/language/class_template#Explicit_instantiation, this needs to be here in the header otherwise the compiler
		*	doesn't know properly the classes used with this template :-(.
		*/
		template <class T> T* GetSystem()
		{
			if (exiting)
				return nullptr;
			
			for (auto &ptr : systemPointers)
			{
				if (typeid(*ptr) == typeid(T))
				{
					auto _ptr = dynamic_cast<T*>(ptr.get());
					return _ptr;
				}
			}

			return nullptr;
		}

		void OnRendererPresentComplete();

	private:
		std::mutex padlock;

		inline static std::shared_ptr<GameEngine> instance = nullptr;

		const char *engineFileDirectory = "../Game";
		bool debugBuild = false;
		bool engineReady = false;

		std::atomic<long long> lastDeltaTime = 0;
		std::atomic<long long> currentDeltaTime = 0;
		std::atomic<long long> lastUpdatedDeltaTime = 0;
		std::atomic<long long> currentTime = 0;
		std::atomic<long long> appStartTime = 0;
		std::atomic<bool> exiting = false;

		HWND gameWindowHwnd = 0;
		HWND debugConsoleWindowHwnd = 0;

		std::thread threadUpdateDebugManager;
		std::thread threadPerformRender;

		std::thread threadMainUpdateLoop;

		std::vector<std::shared_ptr<EngineSystem>> systemPointers;

		const char *appName = "";
		const char* engineBuildDate = "";
		const char* engineBuildTime = "";
		const char* gameBuildDate = "";
		const char* gameBuildTime = "";

		RECT gameWindowRect;
		Vector2 gameWindowDimensions;

		/*
		* std::shared_ptr isn't compatible with COM interfaces, so we need to use ComPtr instead (exactly the same thing, just handled differently internally).
		* See https://github.com/Microsoft/DirectXTK/wiki/ComPtr
		*/
		ComPtr<IDXGISwapChain> swapchainInterface = nullptr;
		ComPtr<ID3D11Device> deviceInterface = nullptr;
		ComPtr<ID3D11DeviceContext> deviceContext = nullptr;

		void InitWindow();
		void CentralUpdateLoop();

		bool InitDirectX(const bool& enableDeviceDebug = true);
	};
}

#endif