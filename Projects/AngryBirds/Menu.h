/*
	This class is responsible for all menu interpretation.

	Created : 14th February 2018 @ 16:37
	Updated: June 2021
	Author : Alexander Stopher
*/

#ifndef MENU_H
#define MENU_H

#include <Engine/Sprite.h>
#include <GameObject.h>
#include "Game.h"

class Menu
{
public:
	Menu();
	~Menu();

	enum menus
	{
		MENU_MAINMENU = 0,
		MENU_INGAME = 1,
		MENU_LEVELSELECTION = 2,
		MENU_PAUSED = 3,
		MENU_LEVELOVER = 4,
		MENU_PROFILES = 5,
		MENU_HOW_TO_PLAY = 6,
		MENU_HIGHSCORES = 7,
	};
	enum states { STATE_NORMAL = 0, STATE_HOVER = 1, STATE_DISABLED = 2 };

	struct menu_item
	{
		GameObject* game_object = new GameObject;
		double x_pos;
		double y_pos;
		states state = states::STATE_NORMAL;
		std::string texture_file_location_1;
		std::string texture_file_location_2;
		std::string action;
	};

	struct menu
	{
		menus name;
		menu_item objects[32] = {};
	};

	bool initialize(std::unique_ptr<ASGE::Renderer>* renderer, Scene* scene, Level* level); //Initialize the menu "engine".
	int num_of_menus = 0;
	menu menu_storage[32] = {}; //An array of menus.

	menu_item* getMenu(menus menu); //Returns an array of menu items that the renderer will render.
	void processClick(Mouse* mouse, Scene* scene, Level* level, const bool& in_menu, const bool& paused, const bool& in_level_selection, Audio* audio);
	menus getCurrentMenu();
	void setCurrentMenu(Menu::menus menu);

	void updateLevelSelectionMenu(float y_offset, const ASGE::GameTime& us);
	bool hasLoaded();

	float scroll_pos = 122.0f;

private:
	std::unique_ptr<ASGE::Renderer>* renderer = nullptr;
	Scene* scene = nullptr;
	Level* level = nullptr;

	menus current_menu;
	bool loaded = false;
	bool hasStartedInit = false;
};

#endif