/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Main Entry Point (Game)
*/

#include <iostream>
#include <string>
#include <thread>

#include "GameEngine.h"
#include "Game.h"

using namespace Charon;

class OnEngineIntermediateInitialisation : public Delegate
{
private:
	void Callback() override;
};

class OnEngineInitialisationComplete : public Delegate
{
private:
	void Callback() override;
};