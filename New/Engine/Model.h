/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Model (Header).
*/

#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <memory>
#include <DirectXMath.h>

#include "Vertex.h"
#include "Matrix.h"

using namespace Charon::Mathematics;
using namespace GameInterface;

namespace Charon
{
	class Model {
	public:
		Model(const std::vector<Vertex>& vertices) : verts(vertices) { worldMatrix = std::make_shared<Matrix>(); };
		~Model() = default;
		
		std::vector<Vertex> GetModelVertices() { return verts; }

		void Rotate(const Charon::Mathematics::Vector3& rotation);
		void Rotate(const Charon::Mathematics::Vector4& quaternion);
		void RotateAndMove(const Charon::Mathematics::Vector3& rotation, const Charon::Mathematics::Vector3& position);
		void LookAt(const Charon::Mathematics::Vector3& position);
		void SetPosition(const float& xPos, const float& yPos, const float& zPos);
		void SetPosition(const Charon::Mathematics::Vector3& pos);
		void SetScale(const Charon::Mathematics::Vector3& scale);
		
		void SetScale(const float& scale);
		void SetScale(const float& x, const float& y, const float& z);

		Charon::Mathematics::Vector3 GetPosition();
		Charon::Mathematics::Vector3 GetRotation();
		Charon::Mathematics::Vector3 GetScale();

		Matrix* GetWorldMatrix() { return worldMatrix.get(); }
	private:
		std::shared_ptr<Matrix> worldMatrix = nullptr;

		std::vector<Vertex> verts;
	};
}

#endif