/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Engine system superclass (Header).
*/

#ifndef ENGINESYSTEM_H
#define ENGINESYSTEM_H

#include <thread>
#include <mutex>

namespace Charon
{
	class EngineSystem
	{
	public:
		EngineSystem() = default;
		~EngineSystem() = default;

		virtual void Initialise() { };

		virtual void OnQuit() { };
		virtual void OnUpdate(const long long delta_t) { };
	protected:
		std::mutex lockGuard;
	};
}
#endif