/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Pixel Shader File.
* 
*   NOTE: The contents of this file is third-party, from http://www.directxtutorial.com/Lesson.aspx?lessonid=11-4-5.
*/

struct VOut
{
    float4 position : SV_POSITION;
    float4 colour : COLOR;
};

float4 main(float4 position : SV_POSITION, float4 colour : COLOR) : SV_TARGET
{
    return colour;
}