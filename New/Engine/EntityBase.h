/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Entity Base (Header).
*/

#ifndef ENTITYBASEDEF_H
#define ENTITYBASEDEF_H

#include <vector>
#include <string>
#include <memory>

#include "Vector3.h"

namespace GameInterface {
	class EntityBase
	{
	public:
		EntityBase(const unsigned int& _entityId, const std::string &_name, EntityBase* parent) : entityId(_entityId), name(_name), parentEntity(parent) { }
		virtual ~EntityBase() { OnDestroy(); }

		virtual void OnDestroy() { };
		virtual void OnAwake() { };
		virtual void OnStart() { };
		virtual void OnUpdate() { };
		virtual void OnLateUpdate() { };
		virtual void OnFixedUpdate() { };
		virtual void OnEnabled() { };
		virtual void OnDisabled() { };

		void SetActive(const bool& _active) { active = _active; if (active) OnEnabled(); else OnDisabled(); }
		void SetParent(EntityBase* parent) { parentEntity = parent; }
		void AddChild(EntityBase* child);
		void RemoveChild(EntityBase* child);

		EntityBase* GetParent() { return parentEntity; }
		
		std::string GetName() { return name; }

		unsigned int GetId() { return entityId; }
		
		/*
		* If this is different to GetState, then we know it's just been enabled/disabled and we can trigger the correct
		*	methods (e.g. OnEnabled, OnDisabled).
		*/
		bool GetPreviousActiveState() { return prevActiveState; }
		bool GetState() { return active; }

	protected:
		bool active = false;
		bool prevActiveState = false;

		unsigned int entityId = 0;
		std::string name;

		/*
		*	Components are subsystems that share the same EntityBase superclass and are owned by the owner EntityBase.
		*	Child entities are separate entities that are childed to this entity, and when the parent moves/rotates/enabled/disabled the same
		*		is applied to all children.
		* 
		*	Children can be moved to another entity, but components cannot.
		*/
		std::vector<EntityBase*> childEntities;
		EntityBase* parentEntity = nullptr;
	};
}
#endif