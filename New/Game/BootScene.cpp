/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Boot Scene Manager
*/

#include "GameEngine.h"

#include "BootScene.h"
#include "GameManager.h"

BootScene::BootScene()
{
	engineScene = GameEngine::GetInstance()->GetSystem<SceneManager>()->CreateNewScene("Boot", true);

	engineScene->AddEntity<Camera>("MainCamera");
	engineScene->AddEntity<GameManager>("GameManager");

	engineScene->GetCamera()->SetActive(true);
	engineScene->GetCamera()->GetProjectionMatrix()->SetLookAtPosition(Vector3Backward);

	engineScene->FindEntityByName("GameManager")->SetActive(true);
}