/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Render Manager (Code File).
*/


#include "RenderManager.h"

Charon::RenderManager::RenderManager(IDXGISwapChain** _swapchainInterface, ID3D11Device** _deviceInterface, ID3D11DeviceContext** _deviceContext) :
	swapchainInterface(*_swapchainInterface), deviceInterface(*_deviceInterface), deviceContext(*_deviceContext) {
	ID3D11Texture2D* tmpBackBuffer;

	std::cout << "Charon : Renderer initialising." << std::endl;

	swapchainInterface->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&tmpBackBuffer);
	deviceInterface->CreateRenderTargetView(tmpBackBuffer, NULL, &backBuffer);
	tmpBackBuffer->Release();

	deviceContext->OMSetRenderTargets(1, &backBuffer, NULL);

	vertexFactory = std::make_unique<VertexFactory>();

	sceneManager = GameEngine::GetInstance()->GetSystem<SceneManager>();

	std::cout << "Charon : Retrieved and set back buffers." << std::endl;
}

void Charon::RenderManager::Release()
{
	for (auto& shader : GameEngine::GetInstance()->GetSystem<ShaderManager>()->GetAllShaders())
	{
		if (shader != nullptr)
		{
			if (shader->GetPixelShader() != nullptr) shader->GetPixelShader()->Release();
			if (shader->GetVertexShader() != nullptr) shader->GetVertexShader()->Release();
		}
	}

	swapchainInterface->SetFullscreenState(FALSE, NULL);
	backBuffer->Release();
	deviceInterface->Release();
	deviceContext->Release();
}

void Charon::RenderManager::OnUpdate(const long long delta_t) {

	fpsUpdateTimer += delta_t;
	//std::cout << "fps update timer: " << fpsUpdateTimer << ", last stored fps: " << lastStoredFps << ", fps: " << framesPerSecond << "\n";


	if (fpsUpdateTimer >= 1000)
	{
		fpsUpdateTimer = 0;
		totalFps = 0;

		lastStoredFps = framesPerSecond;
		framesPerSecond = totalFps - lastStoredFps;
	}
}

void Charon::RenderManager::RequestRender() {
	while (!GameEngine::GetInstance()->IsExiting()) // Force the thread to keep alive even if the engine isn't ready yet.
	{
		std::lock_guard<std::mutex> lock(lockGuard);

		if (sceneManager != nullptr && GameEngine::GetInstance()->IsEngineReady())
		{
			Scene* activeScene = sceneManager->GetActiveScene();

			// No scene active? Don't bother trying to render!
			if (activeScene != nullptr)
			{
				for (std::shared_ptr<EntityBase> entity : activeScene->GetAllEntities())
				{
					if (entity != nullptr)
					{
						if (entity->GetState())
							entity->OnFixedUpdate();
					}
				}

				// This is used to make sure the renderer stays in step with the rest of the engine, so it doesn't Present() until the engine is ready (not to be confused with
				//	telling it to render x times per second, because we aren't responsible for that (DirectX is)).
				//
				// Also checks if the active scene has a camera set, because there's no point in rendering if there's no projection matrix!
				if (readyToRender && graphicsInitComplete && (activeScene->GetCamera() != nullptr))
				{
					// Don't render if the scene's camera is inactive!
					if (activeScene->GetCamera()->GetState())
					{
						readyToRender = false;

						unsigned int currentIndex = 0;
						std::vector<Charon::Model*> models = vertexMaps[RENDER_OBJECT_TYPE::TYPE_TRIANGLES];

						// D3D11.1, https://stackoverflow.com/a/31150249
						deviceContext->ClearRenderTargetView(backBuffer, RGBA{ 0, 0, 0, 0 });

						// Support different cameras for different objects.
						// TODO: Add functionality to render a single object to multiple cameras!

						GameInterface::Camera* camera = Charon::GameEngine::GetInstance()->GetSystem<SceneManager>()->GetActiveScene()->GetCamera();

						for (auto& model : models)
						{
							ConstantBuffer tmp{};
							tmp.worldViewProjectionMatrix = model->GetWorldMatrix()->GetMatrix({ camera->GetViewMatrix(), camera->GetProjectionMatrix() });

							deviceContext->UpdateSubresource(constantBuffer.Get(), 0, NULL, &tmp, 0, 0);
							deviceContext->VSSetConstantBuffers(0, 1, constantBuffer.GetAddressOf());
							deviceContext->DrawIndexed(static_cast<UINT>(model->GetModelVertices().size()), currentIndex, static_cast<UINT>(0));

							currentIndex += static_cast<unsigned int>(model->GetModelVertices().size());
						}

						if (swapchainInterface->Present(vsyncEnabled ? 1 : 0, 0) == S_OK)
						{
							totalFps++;

							if (bufferUpdateRequired && !currentlyUpdatingBuffers)
							{
								currentlyUpdatingBuffers = true; // This is a separate thread so can be called many times while this is happening.

								UpdateBuffers(RENDER_OBJECT_TYPE::TYPE_TRIANGLES);
								UpdateConstantBuffer();

								currentlyUpdatingBuffers = false;
								bufferUpdateRequired = false;
							}

							GameEngine::GetInstance()->OnRendererPresentComplete();
						}
					}
				}
			}
		}
	}

	// This can only be reached when the engine is shutting down, so terminate the thread.
	std::exit(0);
}

void Charon::RenderManager::CreateShader(ShaderDef* shader)
{
	if (shader != nullptr)
	{
		if (shader->GetCompiledShader() != nullptr)
		{
			SHADER_TYPE shaderType = shader->GetShaderType();

			if (shaderType == SHADER_TYPE::SHADER_PIXEL)
			{
				deviceInterface->CreatePixelShader(shader->GetCompiledShader()->GetBufferPointer(), shader->GetCompiledShader()->GetBufferSize(), NULL, shader->GetPixelShaderAddr());
				deviceContext->PSSetShader(shader->GetPixelShader(), NULL, NULL);

				pixelShader = shader;
			}
			else
			{
				deviceInterface->CreateVertexShader(shader->GetCompiledShader()->GetBufferPointer(), shader->GetCompiledShader()->GetBufferSize(), NULL, shader->GetVertexShaderAddr());
				deviceContext->VSSetShader(shader->GetVertexShader(), NULL, NULL);

				vertexShader = shader;
			}
		}
	}
}

void Charon::RenderManager::InitGraphics()
{
	if (vertexShader)
	{
		if (vertexShader->GetCompiledShader())
		{
			vertexSize = sizeof(Vertex);
			D3D11_INPUT_ELEMENT_DESC inputLayoutArray[] =
			{
				{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
				{"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
				{"COLOR", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0},
			};

			vertexMaps.insert(std::pair<Charon::RENDER_OBJECT_TYPE, std::vector<Model*>>(RENDER_OBJECT_TYPE::TYPE_TRIANGLES, std::vector<Model*>()));

			mappedVertexBuffers.insert(std::pair<Charon::RENDER_OBJECT_TYPE, ComPtr<ID3D11Buffer>>(Charon::RENDER_OBJECT_TYPE::TYPE_TRIANGLES, ComPtr<ID3D11Buffer>()));

			deviceInterface->CreateInputLayout(inputLayoutArray, 3, vertexShader->GetCompiledShader()->GetBufferPointer(), vertexShader->GetCompiledShader()->GetBufferSize(), &inputLayout);
			deviceContext->IASetInputLayout(inputLayout);

			ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));
			vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
			vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

			indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
			indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			indexBufferDesc.CPUAccessFlags = 0;
			indexBufferDesc.MiscFlags = 0;

			deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

			D3D11_BUFFER_DESC constantBufferDesc = {};
			constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
			constantBufferDesc.ByteWidth = sizeof(ConstantBuffer);
			constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			constantBufferDesc.CPUAccessFlags = 0;
			constantBufferDesc.MiscFlags = 0;

			if (deviceInterface->CreateBuffer(&constantBufferDesc, NULL, constantBuffer.GetAddressOf()) == S_OK)
			{
				deviceContext->VSSetConstantBuffers(0, 1, constantBuffer.GetAddressOf());
				//deviceContext->PSSetConstantBuffers(0, 1, constantBuffer.GetAddressOf());

				std::cout << "Charon : Constant buffer created, graphics initialisation complete.\n";
				graphicsInitComplete = true;
				return;
			}
			else
				std::cout << "Charon : Constant buffer creation failed!\n";
		}
	}
	
	GameEngine::GetInstance()->Quit(0, "Initialisation Error!", "Graphics init failed! RenderManager::InitGraphics 223.\nExiting.");
}

void Charon::RenderManager::AddModelToBuffer(GameInterface::ThreeDModel* sprite)
{
	vertexMaps[RENDER_OBJECT_TYPE::TYPE_TRIANGLES].push_back(sprite->GetModel());
	bufferUpdateRequired = true;
}

void Charon::RenderManager::RemoveModelFromBuffer(GameInterface::ThreeDModel* sprite)
{
	std::erase(vertexMaps[RENDER_OBJECT_TYPE::TYPE_TRIANGLES], sprite->GetModel());
	bufferUpdateRequired = true;
}

void Charon::RenderManager::UpdateBuffers(const Charon::RENDER_OBJECT_TYPE& objectType)
{
	/*
	*	Verts are assigned to one of three vertex buffers depending on what they are  (2D, 3D, or UI), so we only
	*		need to update the correct one (e.g. no need to update everything if something's only changed in the UI,
	*		for instance if the game is paused we don't need to update the 3D and 2D verts, but only the UI verts).
	*/
	unsigned int tmpVertCount = 0;
	unsigned int totalVerts = 0;
	//RENDER_OBJECT_TYPE bufferType;
	std::vector<Model*> Models;
	std::vector<UINT> vBufferOffsets;
	std::vector<ComPtr<ID3D11Buffer>> tmpVertBufferContainer;
	std::vector<unsigned int> indices;
	std::vector<Vertex> verts;

	ID3D11Buffer* mappedBuffer = GetVertexBuffer(objectType);
	Models = vertexMaps.at(objectType);

	for (auto& model : GetModels(RENDER_OBJECT_TYPE::TYPE_TRIANGLES))
	{
		tmpVertCount += static_cast<unsigned int>(model->GetModelVertices().size());
		for (Vertex& vert : model->GetModelVertices())
			verts.push_back(vert);
	}

	for (auto& vertMap : vertexMaps)
	{
		for (auto& vertGroup : vertMap.second)
			totalVerts += static_cast<unsigned int>(vertGroup->GetModelVertices().size());

		vBufferOffsets.push_back(0);
	}

	// After enumerating the verts in the buffer

	vertexBufferDesc.ByteWidth = vertexSize * tmpVertCount;

	if (deviceInterface->CreateBuffer(&vertexBufferDesc, NULL, &mappedBuffer) != S_OK)
	{
		std::cout << "Charon : Critical error, vertex pipeline buffer creation pass failed (CreateBuffer failed)!\n";
		// Error!
	}

	if (mappedBuffer == nullptr)
	{
		std::cout << "Charon : Critical error, vertex pipeline buffer creation pass failed (buffer is nullptr)!\n";
	}

	// Remap all the vertex buffers.

	for (auto& vertBuf : mappedVertexBuffers)
		tmpVertBufferContainer.push_back(vertBuf.second);

	vertexSize = static_cast<UINT>(sizeof(Vertex) * vBufferOffsets.size());
	deviceContext->IASetVertexBuffers(0, 1, &mappedBuffer, &vertexSize, &offset);

	deviceContext->Map(mappedBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);
	memcpy(ms.pData, verts.data(), tmpVertCount * sizeof(Vertex));
	deviceContext->Unmap(mappedBuffer, NULL);

	// Create the data for the index buffer, because we still need to rebuild it for all vertex buffers every time.
	for (unsigned int i = 0; i < totalVerts; i++)
		indices.push_back(i);

	ZeroMemory(&indexBufferIndiciesData, sizeof(indexBufferIndiciesData)); // This freaks out if we don't ZeroMemory.
	indexBufferDesc.ByteWidth = sizeof(unsigned int) * totalVerts;
	indexBufferIndiciesData.SysMemPitch = 0;
	indexBufferIndiciesData.pSysMem = indices.data();
	indexBufferIndiciesData.SysMemSlicePitch = 0;

	deviceInterface->CreateBuffer(&indexBufferDesc, &indexBufferIndiciesData, indexBuffer.GetAddressOf());
	deviceContext->IASetIndexBuffer(indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
}

void Charon::RenderManager::UpdateConstantBuffer()
{
	Vector2 gameWindowDimensions = GameEngine::GetInstance()->GetGameWindowDimensions();

	GameInterface::Camera* camera = sceneManager->GetActiveScene()->GetCamera();

	if (camera->GetState())
	{
		camera->GetViewMatrix()->CreateViewMatrix();

		camera->GetProjectionMatrix()->SetPerspective((gameWindowDimensions.x / gameWindowDimensions.y), Vector2{ 0.1f, 100.0f }, 90);

		deviceContext->UpdateSubresource(constantBuffer.Get(), NULL, NULL, &constantData, NULL, NULL);

		DirectX::XMFLOAT3 newCameraPos = camera->GetProjectionMatrix()->GetPosition();
		std::cout << "DEBUG: Camera pos is: " << newCameraPos.x << ", " << newCameraPos.y << ", " << newCameraPos.z << "\n";
	}
	else
		std::cout << "DEBUG: a camera was found but it is not yet active! Nothing will be rendered.\n";
}

ID3D11Buffer* Charon::RenderManager::GetVertexBuffer(const Charon::RENDER_OBJECT_TYPE& objectType)
{
	for (auto& buf : mappedVertexBuffers)
	{
		if (buf.first == objectType)
			return buf.second.Get();
	}

	return nullptr;
}

// objectType is unused for now
std::vector<Charon::Model*> Charon::RenderManager::GetModels(const Charon::RENDER_OBJECT_TYPE& objectType)
{
	std::vector<Charon::Model*> models;

	if (sceneManager != nullptr)
	{
		/*
		* Loops through every entity in the active scene, and for each entity check if it is a ThreeDModel pointer
		*	(because all entities are stored as the EntityBase base class and then expanded to their actual class
		*	when needed, like here).
		* If it is a ThreeDModel pointer we know it has a GetModel function and we can therefore get its model.
		*
		* Not every entity will be a model, since the entity system is designed to support any game system within
		*	a scene, such as the game manager and player control classes!
		*/
		for (auto& ent : sceneManager->GetActiveScene()->GetAllEntities())
		{
			try
			{
				if (dynamic_cast<GameInterface::ThreeDModel*>(ent.get()))
				{
					dynamic_cast<GameInterface::ThreeDModel*>(ent.get());
					models.push_back(dynamic_cast<GameInterface::ThreeDModel*>(ent.get())->GetModel());
				}
			}
			catch (int e) {};
		}
	}

	return models;
}
