/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Preferences Manager (Header)
*/

#ifndef PREFSMANAGER_H
#define PREFSMANAGER_H

#include <map>
#include <any>
#include <utility>
#include <string>
#include <mutex>

#include "Enums.h"

namespace Charon
{
	class PrefsManager
	{
	public:
		PrefsManager();

		std::any GetPref(std::string name, RESULT_CODE& resultCode);
		std::any GetPref(std::string name);
		
		bool GetPrefExists(std::string name);
		bool SetPref(std::string name, std::any value);

		void OnQuit();

	private:
		std::mutex mtxLock;
		std::map<std::string, std::any> prefs;

		void SavePrefs();
		void LoadPrefs();
	};
}
#endif