#include <GameObject.h>
#include "Mathematics.h"

//Returns the distance (in pixels) between two given objects, uses Pythagoras' Theorem.
double Mathematics::getDistance(ASGE::Sprite *a, ASGE::Sprite *b)
{
	double a_x = a->xPos();
	double a_y = a->yPos();
	double b_x = b->xPos();
	double b_y = b->yPos();

	double difference_x = a_x - b_x;
	double difference_y = a_y - b_y;

	return std::sqrt((difference_x * difference_x) + (difference_y * difference_y));
}

//Returns the distance (in pixels) between two given objects, uses pos object.
double Mathematics::getDistance(pos a, pos b)
{
	double difference_x = a.x - b.x;
	double difference_y = a.y - b.y;

	return std::sqrt((difference_x * difference_x) + (difference_y * difference_y));
}

//Same as above, but manually define the x and y of both objects. Overloads getDistance(GameObject *a, GameObject *b).
double Mathematics::getDistance(double a_x, double a_y, double b_x, double b_y)
{
	double difference_x = a_x - b_x;
	double difference_y = a_y - b_y;

	return std::sqrt((difference_x * difference_x) + (difference_y * difference_y));
}

double Mathematics::radToDeg(double radians)
{
	return (180.0 * radians) / 3.14;
}

double Mathematics::degToRad(double degrees)
{
	return (degrees * 3.14) / 180;
}

//As above, but returns the distance between two points. Used for distance on the same axis (not really needed since it's simply a-b but it's here for the sake of completion).
double Mathematics::getDistance(double a, double b)
{
	return a - b;
}

//Calculates the angle between two points and returns in degrees.
float Mathematics::getAngle(ASGE::Sprite *a, ASGE::Sprite *b)
{
	return (180.0 * std::atan2(a->yPos() - b->yPos(), a->xPos() - b->xPos())) / 3.14;
}

float Mathematics::getAngleInRadians(ASGE::Sprite *a, ASGE::Sprite *b)
{
	return std::atan2(a->yPos() - b->yPos(), a->xPos() - b->xPos());
}