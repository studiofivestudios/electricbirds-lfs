/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	File System Manager (Header)
*/

#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <vector>
#include <shlwapi.h>
#include <filesystem>
#include <mutex>

#include "EngineSystem.h"
#include "GameEngine.h"
#include "File.h" 

namespace Charon {
	class FileSystemManager : public EngineSystem
	{
	public:
		FileSystemManager() = default;
		~FileSystemManager() = default;

		File* LoadFile(const char* fileName, const bool& keepOpen, const bool& relativeToGame = true);
		char* LoadAndGetContents(const char* fileName, const bool& keepOpen, const bool& relativeToGame = true);

		bool WriteFile(const char* fileName, const unsigned char* fileContents);
		bool IsFileAlreadyLoaded(const char* fileName);

		std::string GetExecutableDirectory();

		std::vector<std::string> EnumerateDirectory(const char* directory, const bool& relativeToGame = true);
	private:
		std::mutex lock;
		std::vector<std::unique_ptr<File>> files;
	};
}

#endif