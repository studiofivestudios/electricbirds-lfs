/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Model (Code).
*/

#include "Model.h"

void Charon::Model::Rotate(const Charon::Mathematics::Vector3& rotation)
{
	worldMatrix->SetRotation(rotation);
}

void Charon::Model::Rotate(const Charon::Mathematics::Vector4& quaternion)
{
	// TODO
}

void Charon::Model::RotateAndMove(const Charon::Mathematics::Vector3& rotation, const Charon::Mathematics::Vector3& position)
{
	Rotate(rotation);
	worldMatrix->SetPosition(position.x, position.y, position.z);
}

void Charon::Model::LookAt(const Charon::Mathematics::Vector3& position)
{
	worldMatrix->SetLookAtPosition(position);
}

void Charon::Model::SetPosition(const float& xPos, const float& yPos, const float& zPos)
{
	worldMatrix->SetPosition(xPos, yPos, zPos);
}

void Charon::Model::SetPosition(const Charon::Mathematics::Vector3 &pos)
{
	worldMatrix->SetPosition(pos.x, pos.y, pos.z);
}

void Charon::Model::SetScale(const float& scale)
{
	worldMatrix->SetScale(Vector3(scale, scale, scale));
}

void Charon::Model::SetScale(const float& x, const float& y, const float& z)
{
	worldMatrix->SetScale(Vector3(x, y, z));
}

void Charon::Model::SetScale(const Vector3& scale)
{
	worldMatrix->SetScale(scale);
}

Charon::Mathematics::Vector3 Charon::Model::GetPosition()
{
	DirectX::XMFLOAT3 matrixPos = worldMatrix->GetPosition();
	return Vector3{ matrixPos.x, matrixPos.y, matrixPos.z };
}

Charon::Mathematics::Vector3 Charon::Model::GetRotation()
{
	DirectX::XMFLOAT3 matrixPos = worldMatrix->GetRotation();
	return Vector3{ matrixPos.x, matrixPos.y, matrixPos.z };
}

Charon::Mathematics::Vector3 Charon::Model::GetScale()
{
	DirectX::XMFLOAT3 matrixPos = worldMatrix->GetScale();
	return Vector3{ matrixPos.x, matrixPos.y, matrixPos.z };
}