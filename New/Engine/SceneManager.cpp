#include "SceneManager.h"

Charon::Scene* Charon::SceneManager::CreateNewScene(const std::string& name, const bool& setActive)
{
    if (GetScene(name))
    {
        std::cout << "Charon : Scene Manager cannot create the requested scene as one of the same name already exists: " << name << "\n";
        return nullptr;
    }

    scenes.insert(std::pair<std::string, std::shared_ptr<Scene>>(name, std::make_shared<Scene>(name)));
    std::cout << "Charon : Scene Manager created the scene: " << name << "\n";

    if (setActive)
        SetActiveScene(name);

    return scenes.at(name).get();
}

void Charon::SceneManager::SetActiveScene(const std::string& name)
{
    Scene* activeScene = GetActiveScene();

    if (activeScene != nullptr)
        activeScene->Deactivate();

    GetScene(name)->Activate();
}

void Charon::SceneManager::SetActiveScene(Scene* scene)
{
    Scene* activeScene = GetActiveScene();
    if (activeScene != nullptr)
        activeScene->Deactivate();

    scene->Activate();

    std::cout << "Charon : Scene Manager has changed the active scene: " << scene->GetName() << "\n";
}

Charon::Scene* Charon::SceneManager::GetActiveScene()
{
    for (auto& scene : scenes)
    {
        if (scene.second.get()->IsActive())
            return scene.second.get();
    }

    return nullptr;
}

Charon::Scene* Charon::SceneManager::GetScene(const std::string& name)
{
    // Would use .at but .at raises OutOfRange instead of returning nullptr, so we need to iterate :(.
    for (auto& scene : scenes)
    {
        if (scene.first == name)
            return scene.second.get();
    }

    return nullptr;
}

std::vector<Charon::Scene*> Charon::SceneManager::GetAllScenes()
{
    return std::vector<Scene*>();
}
