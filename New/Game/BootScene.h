/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Boot Scene Manager
*/

#ifndef BOOTSCENE_H
#define BOOTSCENE_H

//using namespace Charon;
//using namespace GameInterface;

#include "Scene.h"
#include "ThreeDModel.h"

class BootScene
{
public:
	BootScene();

private:
	Charon::Scene* engineScene = nullptr;
};

#endif

