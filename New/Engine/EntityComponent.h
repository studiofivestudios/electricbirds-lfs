/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Entity Component Superclass (Header).
*/

#ifndef ENTITYCOMPONENTDEF_H
#define ENTITYCOMPONENTDEF_H

#include <vector>
#include <string>
#include <memory>

#include "Vector3.h"
#include "Model.h"

namespace CoreEngine {
	class EntityComponent
	{
	public:
	private:
		GameInterface::EntityBase* parentEntity;
	}
}

#endif