/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Main Entry Point (Game)
*/

#include "Initialisation.h"

int main(int argc, char* argv[]) {
	std::string cmd = "";

	std::cout << "GAME: Initialising game." << std::endl;

	GameEngine::GetInstance()->Initialise(GetConsoleWindow(), "Electric Birds Revisited", __DATE__, __TIME__,
		std::make_shared<OnEngineIntermediateInitialisation>().get(), std::make_shared<OnEngineInitialisationComplete>().get());
}

// Delegate for when the core engine systems have loaded, enough to construct a scene from file and show it on screen (intended to show a loading screen).
void OnEngineIntermediateInitialisation::Callback()
{
	std::cout << "GAME: Engine initialisation has entered intermediate stage (partially loaded).\n";
}

// Delegate for when the engine has finished loading (intended to trigger a switch to menu).
void OnEngineInitialisationComplete::Callback()
{
	std::cout << "GAME: Engine initialisation has entered complete stage (fully loaded).\n";

	std::unique_ptr<Game> game = std::make_unique<Game>();
}