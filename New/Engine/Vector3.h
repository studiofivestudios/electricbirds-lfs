/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	3D Vector (Header).
*/

#ifndef VECTOR3_H
#define VECTOR3_H

namespace Charon::Mathematics {
	struct Vector3
	{
		float x = 0.0f;
		float y = 0.0f;
		float z = 0.0f;

		Vector3 operator +(Vector3 vec)
		{
			return Vector3{ x + vec.x, y + vec.y, z + vec.z };
		}

		Vector3 operator -(Vector3 vec)
		{
			return Vector3{ x - vec.x, y - vec.y, z - vec.z };
		}

		Vector3 operator *(Vector3 vec)
		{
			return Vector3{ x * vec.x, y * vec.y, z * vec.z };
		}

		Vector3 operator /(Vector3 vec)
		{
			return Vector3{ x / vec.x, y / vec.y, z / vec.z };
		}
	};

	static Vector3 Vector3Forward		= Vector3{	0,	0,	1	};
	static Vector3 Vector3Backward		= Vector3{	0,	0,	-1	};
	static Vector3 Vector3Left			= Vector3{	-1,	0,	0	};
	static Vector3 Vector3Right			= Vector3{	1,	0,	0	};
	static Vector3 Vector3Up			= Vector3{	0,	1,	0	};
	static Vector3 Vector3Down			= Vector3{	0,	-1,	0	};
	static Vector3 Vector3Zero			= Vector3{	0,	0,	0	};
	static Vector3 Vector3One			= Vector3{	1,	1,	1	};
}
#endif