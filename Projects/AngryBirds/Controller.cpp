#include "Controller.h"



Controller::Controller()
{
}


Controller::~Controller()
{
}

bool Controller::initialize(std::unique_ptr<ASGE::Renderer> *renderer)
{
	is_connected = false;

	return true;
}

bool Controller::isControllerConnected()
{
	return is_connected;
}

void Controller::onEvent(const GamePadData *data)
{
	if (data->is_connected)
	{
		is_connected = true;
	}
	else
	{
		is_connected = false;
	}
}