/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	4D Vector (Header).
*/

#ifndef VECTOR4_H
#define VECTOR4_H

namespace Charon::Mathematics {
	struct Vector4
	{
		float x = 0.0f;
		float y = 0.0f;
		float z = 0.0f;
		float w = 0.0f;

		Vector4 operator *(float num)
		{
			return Vector4{ x * num, y * num, z * num, w * num };
		}

		Vector4 operator -(float num)
		{
			return Vector4{ x - num, y - num, z - num, w - num };
		}

		Vector4 operator +(Vector4 vec)
		{
			return Vector4{ x + vec.x, y + vec.y, z + vec.z, w + vec.w };
		}
	};
}
#endif