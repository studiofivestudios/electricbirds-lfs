/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
* 
*	Main Entry Point
*/

#include <iostream>
#include <string>
#include <thread>

#include "GameEngine.h"

using namespace CoreEngine;

int main(int argc, char* argv[])
{
	std::string cmd = "";

	std::cout << "ENGINE: Initialising engine." << std::endl;
	gEngine = std::make_unique<GameEngine>();

	std::thread(&GameEngine::Initialise, gEngine, GetConsoleWindow()).join();

	//gEngine->Quit(-1, "Device lost", "The DirectX device has been lost.");

	// Game engine controls when it is allowed to exit with an error code + optional messagem so we can just loop here.
	while (true)
	{
		gEngine->ProcessLoop();
	}
}