/*
	This class is responsible for all game audio and queuing.

	Created : 23rd March 2018 @ 23:05
	Author : Alexander Stopher
*/

#include "Audio.h"
#include "Scene.h"

Audio::Audio()
{
}


Audio::~Audio()
{
	for (int i = 0; i < NUM_QUEUE_SIZE; i++)
	{
		queue[i].instance = nullptr;
		delete(queue[i].instance);
	}
	engine = nullptr;
	delete(engine);
}

bool Audio::initialize(Scene *scene)
{
	scene = scene;
	sf::SoundBuffer *engine = new sf::SoundBuffer();
	if (engine != NULL)
	{
		for (int i = 0; i < NUM_QUEUE_SIZE; i++)
		{
			queue[i].instance = nullptr;
		}
		loaded = true;
	}
	return false;
}

void Audio::onUpdate()
{
	for (int i = 0; i < NUM_QUEUE_SIZE; i++)
	{
		if (queue[i].file_path != NULL)
		{
			//Play audio if queue place is 0, OR if we want to overlap the audio, AND (to both conditions) it is not already playing.
			if (queue[i].instance != nullptr)
			{
				if (queue[i].instance->getStatus() == sf::SoundSource::Stopped && !queue[i].loop)
				{
					if (queue[i].has_played)
					{
						stopAudio(i);
					}
					else
					{
						playAudio(i);
					}
				}
			}
			else if (!queue[i].has_played)
			{
				playAudio(i);
			}
		}
	}
}

int Audio::queueAudio(const char *file_path, bool loop, bool overlap, bool priority)
{
	audio_object sound;
	sound.file_path = file_path;
	sound.loop = loop;
	sound.overlap = overlap;
	sound.priority = priority;
	sound.instance = nullptr;

	sound.pre_muted_level = -1;

	for (int i = 0; i < NUM_QUEUE_SIZE; i++)
	{
		if (queue[i].instance == nullptr)
		{
			queue[i] = sound;
			return i;
		}
	}

	//Return 0 if this has failed for some reason (no space in queue, etc.).
	return 0;
}

void Audio::playAudio(int queue_pos)
{
	sf::Music *music = new sf::Music();
	queue[queue_pos].instance = music;
	queue[queue_pos].instance->setLoop(queue[queue_pos].loop);

	if (queue[queue_pos].instance->openFromFile(queue[queue_pos].file_path))
	{
		if (scene->is_sound_muted)
		{
			queue[queue_pos].pre_muted_level = 100.0f;
			queue[queue_pos].instance->setVolume(0.0f);
		}

		queue[queue_pos].has_played = true;
		queue[queue_pos].instance->play();
	}
}

void Audio::stopAudio(int queue_pos)
{
	queue[queue_pos].instance->stop();
	queue[queue_pos].instance = nullptr;
	delete(queue[queue_pos].instance);
}

void Audio::pauseAudio(int queue_pos)
{
	if (queue[queue_pos].instance != nullptr)
	{
		queue[queue_pos].instance->pause();
	}
}

void Audio::changeVolume(int queue_pos, float desired_vol)
{
	if (queue[queue_pos].instance != nullptr)
	{
		if (queue[queue_pos].instance->getVolume() != desired_vol && queue[queue_pos].pre_muted_level == -1)
		{
			queue[queue_pos].instance->setVolume(desired_vol);
		}
	}
}

void Audio::changeVolumeOfAllPriority(float desired_vol)
{
	for (int i = 0; i < NUM_QUEUE_SIZE; i++)
	{
		if (queue[i].instance != nullptr)
		{
			if (queue[i].priority && queue[i].pre_muted_level == -1)
			{
				if (queue[i].instance->getVolume() != desired_vol)
				{
					queue[i].instance->setVolume(desired_vol);
				}
				
			}
		}
	}
}

int Audio::isFileInQueue(const char *file_name)
{
	for (int i = 0; i < NUM_QUEUE_SIZE; i++)
	{
		if (queue[i].file_path != NULL)
		{
			if (queue[i].file_path == file_name && queue[i].instance != nullptr)
			{
				return i;
			}
		}
	}
	return -1;
}

void Audio::stopAllNonPriority()
{
	for (int i = 0; i < NUM_QUEUE_SIZE; i++)
	{
		if (queue[i].instance != nullptr)
		{
			if (!queue[i].priority)
			{
				stopAudio(i);
			}
		}
	}
}

void Audio::stopAll()
{
	for (int i = 0; i < NUM_QUEUE_SIZE; i++)
	{
		if (queue[i].instance != nullptr)
		{
			stopAudio(i);
		}
	}
}

void Audio::toggleMute(bool mute)
{
	if (mute)
	{
		scene->is_sound_muted = true;

		for (int i = 0; i < NUM_QUEUE_SIZE; i++)
		{
			if (queue[i].instance != nullptr)
			{
				queue[i].pre_muted_level = queue[i].instance->getVolume();
				queue[i].instance->setVolume(0.0f);
			}
		}
	}
	else
	{
		scene->is_sound_muted = false;
		for (int i = 0; i < NUM_QUEUE_SIZE; i++)
		{
			if (queue[i].instance != nullptr)
			{
				if (queue[i].pre_muted_level > -1)
				{
					
					queue[i].pre_muted_level = -1;
					queue[i].instance->setVolume(queue[i].pre_muted_level);
				}
			}
		}
	}
}

bool Audio::hasLoaded()
{
	return loaded;
}