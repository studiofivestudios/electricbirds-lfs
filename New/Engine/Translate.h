/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Mathematics Translation Utilities (Header).
*/

#ifndef TRANSLATE_H
#define TRANSLATE_H

#include <DirectXMath.h>
#include "Vector2.h"
#include "Vector3.h"

namespace Charon::Mathematics {
	class Translate
	{
	public:
		static Charon::Mathematics::Vector3 DegreesToRadians(const Vector3& src);
		static DirectX::XMFLOAT3 DegreesToRadiansXM(const DirectX::XMFLOAT3& src);
		static Charon::Mathematics::Vector3 RadiansToDegrees(const Vector3& src);
		static DirectX::XMFLOAT3 RadiansToDegreesXM(const DirectX::XMFLOAT3& src);
	};
}
#endif