/*
	This struct is responsible for mathematics operations.

	This is a struct since Mathematics::pos is widely used as a type in physics/placement calculations, and as a class using 'Mathematics::pos' as a variable type
	(such as 'Mathematics::pos bezier_trajectory[bezier_resolution];') where 'Mathematics' is a class is not possible in C++ (I can assign the type to a variable
	 and use that, but not directly).

	Created : 23rd February 2018 @ 16:33
	Updated: June 2021
	Author : Alexander Stopher
*/

#ifndef MATHS_H
#define MATHS_H

struct Mathematics
{
	struct pos
	{
		double x = 0;
		double y = 0;
	};

	static double getDistance(ASGE::Sprite* a, ASGE::Sprite* b);
	static double getDistance(pos a, pos b);
	static double getDistance(double a_x, double a_y, double b_x, double b_y);
	static double getDistance(double a, double b);
	static double radToDeg(double radians);
	static double degToRad(double degrees);
	static float getAngle(ASGE::Sprite* a, ASGE::Sprite* b);
	static float getAngleInRadians(ASGE::Sprite* a, ASGE::Sprite* b);
};

#endif