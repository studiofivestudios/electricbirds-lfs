/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Camera (Implementation).
*/

#include "Camera.h"

/*
*	TBD!
* 
*	Also known as "picking", gets the position within the camera of the specified entity.
*/
GameInterface::Camera::Camera(const unsigned int& _entityId, const std::string& _name, EntityBase* parent) : EntityBase(_entityId, _name, parent)
{
    viewMatrix = std::make_shared<Matrix>();
    projectionMatrix = std::make_shared<Matrix>();
}

Vector2 GameInterface::Camera::WorldToCamera(EntityBase* entity)
{
    return Vector2{ 0, 0 };
}