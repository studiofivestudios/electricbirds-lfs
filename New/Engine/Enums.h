/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Result Code Storage File (Master Header)
*/

#ifndef RESULTCODES_H
#define RESULTCODES_H

namespace Charon
{
	enum class RESULT_CODE : int
	{
		R_ERROR = 0,
		R_SUCCESS = 1,
	};

	enum class SHADER_TYPE : int
	{
		SHADER_VERTEX,
		SHADER_PIXEL,
		SHADER_NONE
	};
	
	enum class RENDER_OBJECT_TYPE : int
	{
	/*	TYPE_2D,
		TYPE_3D,
		TYPE_UI*/
		TYPE_TRIANGLES
	};
}
#endif