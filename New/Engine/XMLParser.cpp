/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	XML Parser Interface (Code File).
*/

#include "XMLParser.h"

std::shared_ptr<rapidxml::xml_document<>> Charon::XMLParser::ParseXML(const std::string& path)
{
    std::shared_ptr<rapidxml::xml_document<>> doc = std::make_shared<rapidxml::xml_document<>>();

    char* file_buffer = GameEngine::GetInstance()->GetSystem<FileSystemManager>()->LoadAndGetContents(path.c_str(), false);

    if (file_buffer != nullptr)
    {
        doc.get()->parse<rapidxml::parse_no_entity_translation>(file_buffer);
        return doc;
    }

    return nullptr;
}