/*
	This class is responsible for all user profile actions, such as creation, saving and updates.

	Created: 16th April 2018 at 19:26
	Author: Alexander Stopher
*/

#include <sstream>
#include <fstream>
#include <iostream>
#include <regex>

#include "Level.h"
#include "Profiles.h"

Profiles::Profiles()
{
}


Profiles::~Profiles()
{
}

bool Profiles::initialize(Level *level_sys)
{
	level = level_sys;
	int num_of_profiles_loaded = 0;

	for (int i = 0; i <	 MAX_PROFILE_COUNT; i++)
	{
		std::ostringstream f_path;
		f_path << "Resources\\Profiles\\0" << i << ".txt";
		all_profiles[i].f_name = f_path.str();

		if (loadProfile(all_profiles[i]))
		{
			num_of_profiles_loaded++;
		}
	}

	if (num_of_profiles_loaded == 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

std::string Profiles::getNameOfActiveProfile()
{
	return std::string();
}

bool Profiles::addProfile(profile profile_obj)
{
	return false;
}

bool Profiles::deleteProfile(profile profile_obj, std::string backwards_name)
{
	return false;
}

bool Profiles::changeProfileName(profile profile_obj, std::string name)
{
	return false;
}

bool Profiles::createProfile(profile profile_obj)
{
	return false;
}

bool Profiles::loadProfile(profile profile_obj)
{
	return loadProfileFromFile(profile_obj);
}

int Profiles::getAmountOfLoadedProfiles()
{
	return profile_loaded_count;
}

void Profiles::setActiveProfile(profile profile_obj)
{
	restoreGameProgress(profile_obj);

	for (int i = 0; i < MAX_PROFILE_COUNT; i++)
	{
		if (all_profiles[i].f_name != profile_obj.f_name)
		{
			all_profiles[i].is_current = false;
		}
		else
		{
			current = &all_profiles[i];
			all_profiles[i].is_current = true;
			return;
		}
	}
}

bool Profiles::saveProfileChanges(profile profile_obj)
{
	return false;
}

bool Profiles::restoreGameProgress(profile profile_obj)
{
	return false;
}

bool Profiles::loadProfileFromFile(profile profile_obj)
{
	std::string file_path = profile_obj.f_name;
	std::ifstream file(file_path);
	std::string file_contents;
	int level_num = 0;

	std::smatch match;

	for (file_contents; std::getline(file, file_contents);)
	{
		try
		{
			std::regex name(".*?(\".*?\")");
			std::regex level_progress(".*?(\\d+).*?(\\d+).*?(\\d+).*?(\\d+).*?(\\d+)");

			if (std::regex_search(file_contents, match, name) && match.size() > 0)
			{
				profile_obj.name = match.str(1).substr(1, match.str(1).length() - 2);
			}
			if (std::regex_search(file_contents, match, level_progress) && match.size() > 0 && match.str(1).length() > 0)
			{
				level_num++;

				level->level_profile_data[level_num].world = match.str(1);
				level->level_profile_data[level_num].name = match.str(2);
				level->level_profile_data[level_num].score = std::atof(match.str(3).c_str());
				level->level_profile_data[level_num].high_score = std::atof(match.str(4).c_str());
				if (match.str(5) == "\"1\"")
				{
					level->level_profile_data[level_num].locked = true;
				}
				else
				{
					level->level_profile_data[level_num].locked = false;
				}
			}
		}
		catch (std::regex_error e)
		{
			// std's regex throws an error if it can't find a match, but as we're iterating over each line we can ignore any errors regex throws.
		}
	}

	// Profiles are valid if no save-data exists.
	if (profile_obj.name.length() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Profiles::loadAllProfiles()
{
	for (int i = 0; i < MAX_PROFILE_COUNT; i++)
	{
		loadProfileFromFile(all_profiles[i]);
	}
}
