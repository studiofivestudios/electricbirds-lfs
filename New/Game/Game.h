/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Main Game Header
*/

#ifndef GAME_H
#define GAME_H

#include "BootScene.h"
#include "ThreeDModel.h"

class Game
{
public:
	Game();
	~Game() = default;

	bool Quitting() { return quitting; }

private:
	bool quitting = false;

	void Quit() { quitting = true; };
	
	Charon::Scene* loadingScene = nullptr;
	Charon::Scene* mainMenuScene = nullptr;
	Charon::Scene* mainGameScene = nullptr;

	std::unique_ptr<BootScene> bootSceneManager = nullptr;
};

#endif