#include "Cube.h"

#include "Enums.h"
#include "GameEngine.h"

Cube::Cube(const unsigned& _entityId, const std::string& _name, const Vector3& _position,
           const Vector3& _scale, EntityBase* parent): ThreeDModel(entityId, _name, parent)
{
	std::vector<Vector3> tmpVertPositions;

	tmpVertPositions.push_back(Vector3{ 0,0,0 });
	tmpVertPositions.push_back(Vector3{ 0,1,0 });
	tmpVertPositions.push_back(Vector3{ 1,0,0 });
	tmpVertPositions.push_back(Vector3{ 1,1,0 });
	tmpVertPositions.push_back(Vector3{ 1,1,1 });
	tmpVertPositions.push_back(Vector3{ 0,1,0 });
	tmpVertPositions.push_back(Vector3{ 0,1,1 });
	tmpVertPositions.push_back(Vector3{ 0,0,1 });
	tmpVertPositions.push_back(Vector3{ 1,1,1 });
	tmpVertPositions.push_back(Vector3{ 1,0,1 });
	tmpVertPositions.push_back(Vector3{ 1,0,0 });
	tmpVertPositions.push_back(Vector3{ 0,0,1 });
	tmpVertPositions.push_back(Vector3{ 0,0,0 });
	tmpVertPositions.push_back(Vector3{ 0,1,0 });

	GameEngine::GetInstance()->GetSystem<VertexFactory>()->Create(tmpVertPositions, Charon::RENDER_OBJECT_TYPE::TYPE_TRIANGLES, this);
	
	SetPosition(_position);
	SetScale(_scale);
}
