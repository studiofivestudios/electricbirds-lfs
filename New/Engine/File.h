/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	File (Header).
*/

#ifndef FILE_H
#define FILE_H

#include <fstream>
#include <sstream>
#include <iostream>

#include "Enums.h"

namespace Charon {
	class File
	{
	public:
		File(const char* _fileName, const bool &keepOpen = false);
		~File() = default;

		const char* GetContents() { return fileContents.c_str(); };
		std::string GetName() { return fileName; }
		bool GetIsClosedOnDisk();

		void Close() { if (!GetIsClosedOnDisk()) fileHandle.close(); };
		void Open(const bool& appendMode = false);
		void Reload(const bool& appendMode);
		void Write(const unsigned char* contents, const bool &append = false);
	private:
		std::string fileName;
		std::string fileContents;

		std::fstream fileHandle;
	};
}

#endif