/*
	This class is responsible for holding game objects.

	Part of boilerplate code.
*/


#include <Engine\Renderer.h>
#include "..\Projects\AngryBirds\Scene.h"
#include "GameObject.h"

GameObject::~GameObject()
{
	freeSpriteComponent();
}

void GameObject::init(Scene *scene)
{
	scene = scene;
}

bool GameObject::addSpriteComponent(ASGE::Renderer* renderer, const std::string& texture_file_name, Scene *scene)
{
	freeSpriteComponent();

	init(scene);

	sprite_component = new SpriteComponent();
	if (sprite_component->loadSprite(renderer, texture_file_name))
	{
		audioComponent = new Audio();
		file = texture_file_name;

		audioComponent->initialize(scene);
		return true;
	}

	freeSpriteComponent();
	return false;
}

void  GameObject::freeSpriteComponent()
{
	delete sprite_component;
	sprite_component = nullptr;
}


SpriteComponent* GameObject::spriteComponent() 
{ 
	return sprite_component;
}

bool GameObject::resetSprite(ASGE::Renderer* renderer)
{
	if (addSpriteComponent(renderer, file, scene))
	{
		return true;
	}
	return false;
}

std::string GameObject::getTextureFile()
{
	return file;
}

/*
	ASGE appears to have a "bug" where it doesn't check if the desired opacity is less than 0 or greater than 100 before setting the opacity on a sprite.
	This results in undefined behaviour for the sprite's texture where it can sometimes appear blank or setting the opacity doesn't have any effect.

	This function fixes this issue (well, in this project anyway) as it checks if the desired opacity is out-of-bounds before setting it.
*/

void GameObject::setOpacity(float percent)
{
	if (spriteComponent() != nullptr)
	{
		float desired_opacity = percent / 100;

		if ((percent >= 0.0f) || (percent <= 100.0f))
		{
			spriteComponent()->getSprite()->opacity(desired_opacity);
		}
	}
}