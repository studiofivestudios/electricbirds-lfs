/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Delegate Superclass (Header).
*/

#ifndef DELEGATE_H
#define DELEGATE_H

namespace GameInterface
{
	class Delegate
	{
	public:
		virtual void Callback() { };
	};
}

#endif