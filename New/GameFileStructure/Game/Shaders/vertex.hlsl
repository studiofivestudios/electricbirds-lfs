/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Vertex Shader File.
*/

cbuffer MatrixGroup : register(b0)
{
    matrix worldViewProjectionMatrix;
};

struct VOut
{
    float4 position : SV_POSITION;
    float4 colour : COLOR;
};

VOut main(float4 position : POSITION, float4 colour : COLOR)
{
    VOut output;
	
    position.w = 1.0f;

    output.position = mul(position, worldViewProjectionMatrix);
    output.colour = colour;
    
    return output;
}