/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Render Manager (Header).
*/

#ifndef RENDERMANAGER_H
#define RENDERMANAGER_H

#include <vector>
#include <iostream>
#include <d3d11.h>
#include <DirectXMath.h>
#include <unordered_map>
#include <wrl/client.h>

#include "GameEngine.h"
#include "EngineSystem.h" 
#include "ShaderDef.h"
#include "SceneManager.h"
#include "VertexFactory.h"
#include "Model.h"

using namespace Microsoft::WRL;
using namespace Charon::Mathematics;

using RGBA = float[4];

// Suppress warnings about possible erroneous memory locations.
#pragma warning(disable:6387)

namespace Charon {
	class RenderManager : public EngineSystem
	{
	public:
		RenderManager(IDXGISwapChain** _swapchainInterface, ID3D11Device** _deviceInterface, ID3D11DeviceContext** _deviceContext);
		void Release();

		void OnUpdate(const long long delta_t) override;
		void RequestRender();

		void TellCanUpdate() { readyToRender = true; }
		void CreateShader(ShaderDef* shader);
		void SetVSyncEnabled(const bool& enabled) { std::cout << "Charon : VSync has been " << (enabled ? "enabled" : "disabled") << ".\n"; vsyncEnabled = enabled; }
		void InitGraphics();
		void AddModelToBuffer(GameInterface::ThreeDModel *sprite);
		void RemoveModelFromBuffer(GameInterface::ThreeDModel* sprite);

		float GetFramesPerSecond() { return framesPerSecond; };

		VertexFactory* GetVertexFactory() { return vertexFactory.get(); }

		//std::map<unsigned int, Model> GetVertMap() { return verticesGroupsToRender; }
		//Model GetVertGroup(int id) { for (auto vert : verticesGroupsToRender) { if (vert.first == id) return vert.second; } return Model(); }

		bool GetVSyncEnabled() { return vsyncEnabled; }
		bool IsReady() { return graphicsInitComplete; }
	private:
		__declspec(align(16)) // Align to 16 byte boundaries, as per https://stackoverflow.com/a/7522968/2422013.
		struct ConstantBuffer
		{
			DirectX::XMMATRIX worldViewProjectionMatrix;
		};

		IDXGISwapChain *swapchainInterface;
		ID3D11Device *deviceInterface;
		ID3D11DeviceContext *deviceContext;

		/*
		* Normal ptr here because the backbuffer already exists (we just get it and assign a reference of it to this variable).
		* This means that we are not responsible for managing its memory or destroying it.
		*/
		ID3D11RenderTargetView* backBuffer; // TODO: -> ComPtr

		ShaderDef* vertexShader = nullptr;
		ShaderDef* pixelShader = nullptr;

		std::unique_ptr<VertexFactory> vertexFactory = nullptr;

		void UpdateBuffers(const Charon::RENDER_OBJECT_TYPE &objectType);
		void UpdateConstantBuffer();

		ID3D11Buffer* GetVertexBuffer(const Charon::RENDER_OBJECT_TYPE& objectType);

		std::vector<Charon::Model*> GetModels(const Charon::RENDER_OBJECT_TYPE& objectType = Charon::RENDER_OBJECT_TYPE::TYPE_TRIANGLES);

		//ComPtr<ID3D11Buffer> mappedBuffer;
		D3D11_BUFFER_DESC vertexBufferDesc;
		D3D11_BUFFER_DESC indexBufferDesc;
		D3D11_SUBRESOURCE_DATA indexBufferIndiciesData;
		D3D11_MAPPED_SUBRESOURCE ms;

		std::unordered_map<Charon::RENDER_OBJECT_TYPE, ComPtr<ID3D11Buffer>> mappedVertexBuffers;
		ComPtr<ID3D11Buffer> indexBuffer;
		ComPtr<ID3D11Buffer> constantBuffer;

		ConstantBuffer constantData = { };

		ID3D11InputLayout* inputLayout = nullptr;

		SceneManager* sceneManager = nullptr;

		float fpsUpdateTimer = 0;
		float framesPerSecond = 0;
		float lastStoredFps = 0;
		float totalFps = 0;

		UINT vertexSize = 0;
		UINT offset = 0;

		std::unordered_map<Charon::RENDER_OBJECT_TYPE, std::vector<Model*>> vertexMaps;

		std::atomic<bool> readyToRender = false;
		std::atomic<bool> bufferUpdateRequired = false;
		std::atomic<bool> currentlyUpdatingBuffers = false;
		std::atomic<bool> graphicsInitComplete = false;
		std::atomic<bool> vsyncEnabled = true;
	};
}
#endif