/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Cube (Header).
*/

#ifndef CUBE_H
#define CUBE_H
#include "ThreeDModel.h"

namespace GameInterface
{
	class Cube : public ThreeDModel
	{
	public:
		Cube(const unsigned int& _entityId, const std::string& _name, const Vector3& _position = Vector3Zero,
		     const Vector3& _scale = Vector3One,
		     EntityBase* parent = nullptr);
	};
}

#endif // CUBE_H