/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Scene (Header).
*/

#ifndef SCENEDEF_H
#define SCENEDEF_H

#include <string>

#include <vector>
#include <unordered_map>

#include "EntityBase.h"
#include "ThreeDModel.h"
#include "Vertex.h"
#include "Camera.h"

using namespace GameInterface;

namespace Charon {
	class Scene
	{
	public:
		Scene(const std::string& _name) : name(_name) { };
		~Scene() { Destroy(); }

		void RemoveEntityByName(const std::string& name);
		void RemoveEntityById(const unsigned int &id);
		void DisableAllEntities();
		void EnableAllEntities(const bool& restore);

		void SetActiveCamera(Camera* _camera) { camera = _camera; }

		void Activate();
		void Deactivate();
		void Destroy();

		template <class T> T* AddEntity(const std::string& name)
		{
			/*
			* Checks if an entity of the same name exists OR if there is already a camera in the scene- if so, returns nullptr and doesn't create the requested entity.
			* Also checks after this if the object we're trying to add is a camera, and if so we assign it as the scene camera AFTER it's been created.
			* 
			* The camera is then used for the scene by the render manager (because the camera is just a straightforward word for a projection matrix), so
			*	it's important that a camera is added to display anything (no camera = won't render).
			*/
			bool isCamera = typeid(T*) == typeid(Camera*);
			for (auto &ent : entities)
			{
				if ((ent.get()->GetName() == name) || (typeid(ent.get()) == typeid(Camera*) && isCamera))
					return nullptr;
			}

			entities.push_back(std::make_shared<T>(entities.size(), name));

			if (isCamera)
				camera = static_cast<Camera*>(entities[entities.size() - 1].get());

			return static_cast<T*>(entities[entities.size() - 1].get());
		}

		GameInterface::EntityBase* FindEntityByName(const std::string& name);
		GameInterface::EntityBase* FindEntityById(const unsigned int &id);
		GameInterface::Camera* GetCamera() { return camera; }

		std::string GetName() { return name; }

		bool IsActive() { return active; }

		std::vector<Charon::Vertex> GetAllModelVerts(); // Shortcut function, use this instead of iterating through the models manually.
		std::vector<std::shared_ptr<GameInterface::EntityBase>> GetAllEntities() { return entities; }

	private:
		std::shared_ptr<GameInterface::EntityBase> FindEntityByNamePtr(const std::string& name);
		std::shared_ptr<GameInterface::EntityBase> FindEntityByIdPtr(const unsigned int& id);

		std::string name = "";

		bool active = false;
		GameInterface::Camera* camera = nullptr;

		std::vector<std::shared_ptr<GameInterface::EntityBase>> entities;
		std::unordered_map<unsigned int, bool> prevEntityStatesOnDisable;
	};
}

#endif