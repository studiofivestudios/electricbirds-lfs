#include "..\Source\Game.h"

class AngryBirds;

class Blocks
{
public:
	Blocks();
	~Blocks();

	struct block_types {
		int x_pos;
		int y_pos;
		const char *texture_path;
		const float* colour;
	};

	block_types generateBlocks();

private:
	static const int blockArraySize = 8; //Needs to be a static constant as the compiler needs to be sure of the array size at compile time.

	block_types block_formation[blockArraySize];
};