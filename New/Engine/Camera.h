/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Camera (Header).
*/

#ifndef CAMERADEF_H
#define CAMERADEF_H

#include <memory>

#include "EntityBase.h"
#include "Vector2.h"
#include "Matrix.h"

using namespace Charon::Mathematics;

namespace GameInterface {
	class Camera : public EntityBase
	{
	public:
		enum class Dir
		{
			X,
			Z
		};

		Camera(const unsigned int& _entityId, const std::string& _name, EntityBase* parent = nullptr);
		// ReSharper disable once CppVirtualFunctionCallInsideCtor
		~Camera() { OnDestroy(); }

		Vector2 WorldToCamera(EntityBase* entity); // Picking, implementation is TBC and is not implemented.

		/*
		* View: defines where the camera is in the world.
		* Projection: defines the size of the projection (width/height) as well as the view distance.
		* World: on each object (not the camera), defines where the object is in the world.
		*/

		Matrix* GetViewMatrix() { return viewMatrix.get(); }
		Matrix* GetProjectionMatrix() { return projectionMatrix.get(); }

	private:
		std::shared_ptr<Matrix> viewMatrix = nullptr;
		std::shared_ptr<Matrix> projectionMatrix = nullptr;
	};
};
#endif