/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Colour Definition (Header).
*/

#ifndef COLOURDEF_H
#define COLOURDEF_H

namespace Charon {
	struct Colour
	{
		float r;
		float g;
		float b;
		float a = 1.0;
	};
}
#endif