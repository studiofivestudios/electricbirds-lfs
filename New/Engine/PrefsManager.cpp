/*
*	UWE BSc (Hons) Games Technology - Advanced Technologies - Alexander Stopher (17025080)
*
*	Preferences Manager (Code File)
*/

#include "PrefsManager.h"

Charon::PrefsManager::PrefsManager()
{
}

void Charon::PrefsManager::OnQuit()
{
    SavePrefs();
}

std::any Charon::PrefsManager::GetPref(std::string name, RESULT_CODE& resultCode)
{
    /*
    *   Variables are locked here to prevent prefs from being added/removed while the system is using the "prefs" variable, to prevent a situation
    *       where the pref is removed between finding it and returning it (which would cause an error)- this is also why we create the "var" variable
    *       just in case the pref is removed after the mutex is unlocked but before it's returned.
    */

    std::lock_guard<std::mutex> guard(mtxLock);
    if (prefs.find(name) != prefs.end()) {
        std::any var = prefs.find(name)->second;

        resultCode = RESULT_CODE::R_SUCCESS;
        return var;
    }

    resultCode = RESULT_CODE::R_ERROR;
    return -1;
}

std::any Charon::PrefsManager::GetPref(std::string name)
{
    std::lock_guard<std::mutex> guard(mtxLock);

    if (prefs.find(name) != prefs.end()) {
        std::any var = prefs.find(name)->second;
        return var;
    }

    return -1;
}

bool Charon::PrefsManager::GetPrefExists(std::string name)
{
    std::lock_guard<std::mutex> guard(mtxLock);

    return prefs.find(name) != prefs.end();
}

/*
    Sets the given preference.
    If successful (e.g. the pref doesn't already exist), returns true, otherwise returns false.
*/ 
bool Charon::PrefsManager::SetPref(std::string name, std::any value)
{
    std::lock_guard<std::mutex> guard(mtxLock);

    if (GetPrefExists(name)) {
        prefs.insert(std::pair<std::string, std::any>(name, value));
        return true;
    }

    return false;
}

void Charon::PrefsManager::LoadPrefs()
{
    // Not yet implemented (need to add XML library).
}

void Charon::PrefsManager::SavePrefs()
{
    // Not yet implemented (need to add XML library).
}
